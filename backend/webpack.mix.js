const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */

// mix.js('resources/js/app.js', 'public/js')
//     .postCss('resources/css/app.css', 'public/css', [
//         //
//     ]);

mix.js('resources/js/app.js', 'public/js')
mix.styles([
    'resources/css/normalize.css',
    'resources/css/main.css',
    'resources/css/bootstrap.min.css',
    'resources/css/fontawesome-all.min.css',
    'resources/css/flaticon.css',
    'resources/css/animate.min.css',
    'resources/css/magnific-popup.css',
    'resources/css/meanmenu.min.css',
    'resources/css/owl.carousel.min.css',
    'resources/css/owl.theme.default.min.css',
    'resources/css/style.css',
], 'public/css/common.css');
mix.sass('resources/sass/main.scss', 'public/css');
