<?php

use Illuminate\Support\Facades\Route;
use Encore\Admin\Facades\Admin;

use App\Http\Controllers\HomeController;
use App\Http\Controllers\AboutController;
use App\Http\Controllers\ContactController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\TagController;
use App\Http\Controllers\ActressController;
use App\Http\Controllers\SitemapController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
*/

// Route::get('/', function () {
//     return view('welcome');
// });

// Route::fallback(function(){
//     return redirect(route('admin.home'));
// });


Route::get('/', [ HomeController::class, 'index' ])->name('home');
Route::get('post/{id}', [ HomeController::class, 'view' ])->name('single');

Route::get('/about', [ AboutController::class, 'index' ])->name('about');

Route::get('/contact', [ ContactController::class, 'index' ])->name('contact');
Route::post('/contact/confirm', [ ContactController::class, 'confirm' ])->name('contact.confirm');
Route::post('/contact/thanks', [ ContactController::class, 'send' ])->name('contact.send');

Route::get('/category/{id}', [ CategoryController::class, 'archive' ])->name('category');

Route::get('/tag/{id}', [ TagController::class, 'archive' ])->name('tag');

Route::get('/actress/{id}', [ ActressController::class, 'archive' ])->name('actress');

Route::get('/sitemap.xml', [ SitemapController::class, 'index' ])->name('sitemap');