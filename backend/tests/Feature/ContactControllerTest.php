<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Mail\ContactReply;
use Illuminate\Support\Facades\Mail;

class ContactControllerTest extends TestCase
{
    /**
     *
     * @return void
     * @test
     */
    public function routeお問い合わせ()
    {
        $response = $this->get(route('contact'));

        $response->assertStatus(200);
    }

    /**
     * @test
     */
    public function routeお問い合わせ内容確認()
    {
        $response = $this->get(route('contact.confirm'));

        $response->assertStatus(405);
    }

    /**
     * @test
     */
    public function routeサンクスページ()
    {
        $response = $this->get(route('contact.send'));

        $response->assertStatus(405);
    }

    /**
     * @test
     */
    public function routeお問い合わせ内容確認post()
    {
        $response = $this->post(route('contact.confirm'), [
            'name' => 'testName',
            'email' => 'test@example.com',
            'message' => 'post test',
        ]);

        $response->assertStatus(200);
    }

    /**
     * @test
     */
    public function 問い合わせ返信メール送信()
    {
        // 実際にはメールを送らないように設定
        Mail::fake();

        // メールが送られていないことを確認
        Mail::assertNothingSent();

        $action = 'submit';
        $name = 'test';
        $email = 'test@example.com';
        $message = 'message';

        $response = $this->post(route('contact.send'), [
            'action' => $action,
            'name' => $name,
            'email' => $email,
            'message' => $message,
        ]);

         // メッセージが指定したユーザーに届いたことをアサート
        Mail::assertSent(ContactReply::class, function ($mail) use ($email) {
            return $mail->hasTo($email);
        });

        // メールが1回送信されたことをアサート
        Mail::assertSent(ContactReply::class, 1);

    }
}
