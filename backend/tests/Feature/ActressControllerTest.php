<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\Masters\Actress;

class ActressControllerTest extends TestCase
{
    /**
     * @return void
     * @test
     */
    public function route女優()
    {
        $actresses = Actress::limit(10)->get();

        foreach( $actresses as $actress ) {

            $response = $this->get(route('actress', ['id' => $actress->id]));

            $response->assertStatus(200);
        }
    }
}
