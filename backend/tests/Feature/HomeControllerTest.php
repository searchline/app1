<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\Masters\Post;

class HomeControllerTest extends TestCase
{
    /**
     * @return void
     * @test
     */
    public function routeホーム()
    {
        $response = $this->get(route('home'));

        $response->assertStatus(200);
    }

    /**
     * @test
     */
    public function route個別ページ()
    {
        $posts = Post::limit(10)->get();

        foreach( $posts as $post ) {

            $response = $this->get(route('single', ['id' => $post->id]));

            $response->assertStatus(200);

        }
    }

}
