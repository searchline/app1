<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\Masters\VideoSite;

class CategoryControllerTest extends TestCase
{
    /**
     * @return void
     * @test
     */
    public function routeカテゴリー()
    {
        $categories = VideoSite::all();

        foreach( $categories as $category ) {

            $response = $this->get(route('category', ['id' => $category->id]));

            $response->assertStatus(200);
        }
    }
}
