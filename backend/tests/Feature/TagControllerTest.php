<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\Masters\Tag;

class TagControllerTest extends TestCase
{
    /**
     * @return void
     * @test
     */
    public function routeタグ()
    {
        $tags = Tag::limit(10)->get();

        foreach( $tags as $tag ) {

            $response = $this->get(route('tag', ['id' => $tag->id]));

            $response->assertStatus(200);
        }
    }
}
