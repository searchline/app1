<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class SitemapControllerTest extends TestCase
{
    /**
     * @return void
     * @test
     */
    public function routeサイトマップ()
    {
        $response = $this->get(route('sitemap'));

        $response->assertStatus(200);
    }
}
