<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Artisan;
use Illuminate\Support\Facades\Storage;

class PostsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $files = Storage::disk('images')->files();
        Storage::disk('images')->delete($files);
        // Artisan::call('scrape:videosite');
    }
}
