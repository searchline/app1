<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class VideoSitesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = Carbon::now();

        $videoSites = [
            [ 'name' => 'xvideos', 'scrape' => true, 'created_at' => $now, 'updated_at' => $now ],
            [ 'name' => 'Pornhub', 'scrape' => true, 'created_at' => $now, 'updated_at' => $now ],
            [ 'name' => 'JavyNow', 'scrape' => true, 'created_at' => $now, 'updated_at' => $now ],
            [ 'name' => 'ShareVideos', 'scrape' => true, 'created_at' => $now, 'updated_at' => $now ],
            [ 'name' => 'xHamster', 'scrape' => true, 'created_at' => $now, 'updated_at' => $now ],
            [ 'name' => 'RedTube', 'scrape' => true, 'created_at' => $now, 'updated_at' => $now ],
            [ 'name' => 'youjizz', 'scrape' => true, 'created_at' => $now, 'updated_at' => $now ],
            [ 'name' => 'Spankingbang', 'scrape' => true, 'created_at' => $now, 'updated_at' => $now ],
            [ 'name' => 'vjav', 'scrape' => true, 'created_at' => $now, 'updated_at' => $now ],
            [ 'name' => 'Txxx', 'scrape' => true, 'created_at' => $now, 'updated_at' => $now ],
        ];
        DB::table('video_sites')->insert($videoSites);
    }
}
