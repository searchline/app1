<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Masters\Post;
use App\Models\Masters\Tag;
use Illuminate\Support\Facades\DB;
use Faker\Factory as FakerFactory;
use Carbon\Carbon;


class TitlesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        // DB::table('my_service_post')->truncate();

        $posts = Post::all();

        $tagsCount = Tag::count();

        $maxCount = 10;

        $now = Carbon::now();
        $faker = FakerFactory::create('ja_JP');

        foreach( $posts as $post ){

            $post->fill([
                'title' => $faker->realText(80),
                'release_date' => $now,
            ]);
            $post->save();

            $tags = [];
            for( $i=1; $i<=$maxCount; $i++ ){
                while(true) {

                    $tmp = mt_rand(1, $tagsCount);

                    if( ! in_array( $tmp, $tags ) ){
                        array_push( $tags, $tmp );
                        break;
                      }
                }
            }
            $post->tags()->sync($tags);
        }
    }
}
