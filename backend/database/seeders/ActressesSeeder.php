<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Masters\Actress;
use App\Imports\ActressImport;

class ActressesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if(app()->isProduction()){
            $file = database_path('seeders/csv/actresses.csv');
        } else {
            $file = database_path('seeders/csv/actresses_test.csv');
        }
        (new ActressImport)->import($file);
    }
}
