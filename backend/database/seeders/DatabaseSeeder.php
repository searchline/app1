<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            AdminTablesSeeder::class,
            VideoSitesSeeder::class,
            TagsSeeder::class,
            ActressesSeeder::class,
            PostsSeeder::class,
            // TitlesTableSeeder::class,
        ]);
    }
}
