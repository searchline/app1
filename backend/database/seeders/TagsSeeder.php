<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Masters\Tag;
use App\Imports\TagImport;

class TagsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $file = database_path('seeders/csv/tags.csv');
        (new TagImport)->import($file);
    }
}
