<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            // $table->id();
            $table->uuid('id')->primary();
            $table->string('title')->nullable();
            $table->timestamp('release_date')->nullable();
            $table->string('url', 767)->unique();
            $table->string('time')->nullable();
            $table->boolean('check')->default(false);
            $table->boolean('unpublished')->default(false);
            $table->string('filename')->nullable();
            $table->string('thumbnail')->nullable();

            $table->integer('video_site_id')->nullable()->unsigned();
            $table->integer('get_post_history_id')->nullable()->unsigned();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
