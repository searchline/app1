<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGetPostHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('get_post_histories', function (Blueprint $table) {
            $table->id();
            $table->timestamp('date');
            $table->integer('count')->nullable();
            $table->integer('failed_count')->nullable();
            $table->boolean('status')->nullable();
            $table->integer('video_site_id')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('get_post_histories');
    }
}
