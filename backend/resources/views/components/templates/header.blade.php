<!-- Header Area Start Here -->
<header class="has-mobile-menu">
    <div id="rt-sticky-placeholder"></div>
    <div id="header-menu" class="header-menu menu-layout1 bg--dark pr--30 pl--30 container-xl">
        <div class="container">
            <div class="row d-flex align-items-center">
                <div class="col-lg-2 d-flex justify-content-start">
                    <div class="logo-area">
                        <a href="{{ route('home') }}" class="temp-logo" id="temp-logo">
                            @if(\Route::currentRouteName() == 'home' && empty(Request('search')))
                            <h1><img src="{{ asset('img/logo.png') }}" alt="無料動画検索 - エロミネーター" class="img-fluid"></h1>
                            @else
                            <img src="{{ asset('img/logo.png') }}" alt="無料動画検索 - エロミネーター" class="img-fluid">
                            @endif
                        </a>
                    </div>
                </div>
                <div class="col-lg-10 d-flex justify-content-end">
                    <nav id="dropdown" class="template-main-menu">
                        <ul>
                            <li>
                                <a href="{{ route('home') }}">HOME</a>
                            </li>
                            <li>
                                <a href="{{ route('about') }}">当サイトについて</a>
                            </li>
                            <li>
                                <a href="{{ route('contact') }}">お問い合わせ</a>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</header>
<!-- Header Area End Here -->