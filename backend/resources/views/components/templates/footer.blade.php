<!-- Footer Area Start Here -->
<footer class="footer-wrap-layout2">
    <div class="container">
        <div class="footer-box-layout2">
            <div class="footer-logo">
                <a href="{{ route('home') }}"><img src="{{ asset('img/logo.png') }}" alt="logo"></a>
            </div>
            <div class="copyright">© 2021 {{ config('app.name') }}. All Rights Reserved.</div>
        </div>
    </div>
</footer>
<!-- Footer Area End Here -->