@if ($paginator->hasPages())
<div class="pagination-layout1">
    <ul>
        {{-- Previous Page Link --}}
        @if ($paginator->onFirstPage())
            <li>
                  <a class="btn disabled" href="{{ $paginator->url(1) }}" >&lsaquo;&lsaquo;</a>
            </li>
            <li>
                  <a class="btn disabled" href="{{ $paginator->previousPageUrl() }}" >&lsaquo;</a>
            </li>
        @else
            <li>
                  <a href="{{ $paginator->url(1) }}">&lsaquo;&lsaquo;</a>
            </li>
            <li>
                  <a href="{{ $paginator->previousPageUrl() }}">&lsaquo;</a>
            </li>
        @endif

        {{-- Pagination Elements --}}
        @foreach ($elements as $element)
            {{-- "Three Dots" Separator --}}
            @if (is_string($element))
                <!-- <li>
                    <a class="btn disabled">{{ $element }}</a>
                </li> -->
            @endif

            {{-- Array Of Links --}}
            @if (is_array($element))
                @foreach ($element as $page => $url)
                    @if ($page == $paginator->currentPage())
                        <!-- <li class="active"><a href="{{ $url }}">{{ $page }}</a></li> -->
                    @else
                        <!-- <li><a href="{{ $url }}">{{ $page }}</a></li> -->
                    @endif
                @endforeach
            @endif
        @endforeach


        {{-- Pagination Elements --}}
        <?php $num = 8; ?>
        @if ($paginator->lastPage() > $num)

        {{-- 現在ページが表示するリンクの中心位置よりも左の時 --}}
        @if ($paginator->currentPage() <= floor($num / 2))
            <?php $start_page = 1; //最初のページ ?>
            <?php $end_page = $num; ?>

        {{-- 現在ページが表示するリンクの中心位置よりも右の時 --}}
        @elseif ($paginator->currentPage() > $paginator->lastPage() - floor($num / 2))
            <?php $start_page = $paginator->lastPage() - ($num - 1); ?>
            <?php $end_page = $paginator->lastPage(); ?>

        {{-- 現在ページが表示するリンクの中心位置の時 --}}
        @else
            <?php $start_page = $paginator->currentPage() - (floor(($num % 2 == 0 ? $num - 1 : $num)  / 2)); ?>
            <?php $end_page = $paginator->currentPage() + floor($num / 2); ?>
        @endif

        {{-- 定数よりもページ数が少ない時 --}}
        @else
        <?php $start_page = 1; ?>
        <?php $end_page = $paginator->lastPage(); ?>
        @endif

        @for( $i = $start_page; $i <= $end_page; $i++ )
            @if ($i == $paginator->currentPage())
                <li class="active"><a href="{{ $paginator->url($i) }}">{{ $i }}</a></li>
            @else
                <li><a href="{{ $paginator->url($i) }}">{{ $i }}</a></li>
            @endif
        @endfor


        {{-- Next Page Link --}}
        @if ($paginator->hasMorePages())
            <li>
                <a href="{{ $paginator->nextPageUrl() }}" >&rsaquo;</a>
            </li>
            <li>
                  <a href="{{ $paginator->url($paginator->lastPage()) }}" >&rsaquo;&rsaquo;</a>
            </li>
        @else
            <li>
                <a class="btn disabled" href="{{ $paginator->nextPageUrl() }}" >&rsaquo;</a>
            </li>
            <li>
                  <a class="btn disabled" href="{{ $paginator->url($paginator->lastPage()) }}" >&rsaquo;&rsaquo;</a>
            </li>
        @endif
    </ul>
</div>
@endif
