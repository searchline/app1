<script>
window.Laravel = {};
window.Laravel.asset_path = '{{ asset('') }}';
</script>

<!-- jquery-->
<script src="{{ asset('js/jquery-3.3.1.min.js') }}"></script>
<!-- Plugins js -->
<script src="{{ asset('js/plugins.js') }}"></script>
<!-- Popper js -->
<script src="{{ asset('js/popper.min.js') }}"></script>
<!-- Bootstrap js -->
<script src="{{ asset('js/bootstrap.min.js') }}"></script>
<!-- Owl Carousel js -->
<script src="{{ asset('js/owl.carousel.min.js') }}"></script>
<!-- Smooth Scroll js -->
<script src="{{ asset('js/jquery.magnific-popup.min.js') }}"></script>
<!-- Meanmenu js -->
<script src="{{ asset('js/jquery.meanmenu.min.js') }}"></script>
<!-- Main js -->
<script src="{{ asset('js/main.js') }}"></script>