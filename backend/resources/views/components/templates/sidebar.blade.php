<!-- side bar start -->
<div class="col-xl-3 col-lg-4 sidebar-widget-area sidebar-break-md">

    <div class="widget">
        <div class="widget-newsletter-subscribe-dark">
            <form class="newsletter-subscribe-form" method="GET" action="{{ $searchAction }}">
                <div class="form-group">
                    <input type="text" placeholder="キーワード" class="form-control" name="search" value="{{ $search }}">
                    <div class="help-block with-errors"></div>
                </div>
                <div class="form-group mb-none">
                    <button type="submit" class="item-btn">検索</button>
                </div>
            </form>
        </div>
    </div>

    <div class="widget">
        <div class="section-heading heading-dark">
            <h2 class="item-heading">人気の記事</h2>
        </div>
        <div class="widget-popular">
            @foreach( $popularPosts as $post )
            <div class="post-box">
                <div class="item-img">
                    <a href="{{ route('single', [ 'id' => $post->id ] ) }}"><img src="{{ $post->image_path }}" alt="{{ $post->filename }}"></a>
                </div>
                <div class="item-content">
                    <ul class="entry-meta meta-color-dark">
                        <li><i class="fas fa-video"></i>{{ $post->videoSite->name }}</li>
                        <li><i class="fas fa-calendar-alt"></i>{{ $post->list_date }}</li>
                    </ul>
                    <h3 class="item-title"><a href="{{ route('single', [ 'id' => $post->id ] ) }}">{{ $post->title }}</a></h3>
                </div>
            </div>
            @endforeach
        </div>
    </div>

    <div class="widget">
        <div class="section-heading heading-dark">
            <h2 class="item-heading">カテゴリー</h2>
        </div>
        <div class="widget-categories">
            <ul>
                @foreach( $categories as $category )
                    <li>
                        <a href="{{ route('category', [ 'id' => $category->id ]) }}">{{ $category->name }}
                            <span>({{ $category->posts_count }})</span>
                        </a>
                    </li>
                @endforeach
            </ul>
        </div>
    </div>

    <div class="widget">
        <div class="section-heading heading-dark">
            <h2 class="item-heading">タグ一覧</h2>
        </div>
        <div class="widget-categories">
        @foreach( $tags as $tag )
            <a href="{{ route('tag', [ 'id' => $tag->id ]) }}"><span class="badge badge-secondary">{{ $tag->name }}</span></a>
        @endforeach

        </div>
    </div>

</div>
<!-- side bar end -->