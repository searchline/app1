<x-base title="お問い合わせ"
        description=""
>
    <x-slot name="subHeadline"></x-slot>
    <div class="row gutters-50">
        <div class="col-xl-9 col-lg-8">
            <div class="single-blog-box-layout3">
                <div class="single-blog-content">
                    <div class="blog-form">
                        <div class="section-heading-4 heading-dark">
                            <h1 class="item-heading">お問い合わせはこちらから</h1>
                        </div>
                        <form class="contact-form-box contact-form" method="POST" action="{{ route('contact.confirm') }}">
                        @csrf
                            <div class="row gutters-15">
                                <div class="col-12 form-group">
                                    <input type="text" placeholder="お名前*" class="form-control" name="name" value="{{ old('name') }}">
                                    @if ($errors->has('name'))
                                        <div class="invalid-message">{{ $errors->first('name') }}</div>
                                    @endif
                                </div>
                                <div class="col-12 form-group">
                                    <input type="email" placeholder="E-mail*" class="form-control" name="email" value="{{ old('email') }}">
                                    @if ($errors->has('email'))
                                        <div class="invalid-message">{{ $errors->first('email') }}</div>
                                    @endif
                                </div>
                                <div class="col-12 form-group">
                                    <textarea placeholder="お問い合わせ内容*" class="textarea form-control" name="message" rows="9" cols="20">{{ old('message') }}</textarea>
                                    @if ($errors->has('message'))
                                        <div class="invalid-message">{{ $errors->first('message') }}</div>
                                    @endif
                                </div>
                                <div class="col-12 form-group">
                                    <button class="item-btn">入力内容確認</button>
                                </div>
                            </div>
                            <div class="form-response"></div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <x-sidebar/>
    </div>
</x-base>