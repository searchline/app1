<x-base title="当サイトについて"
        description=""
>
    <x-slot name="subHeadline"></x-slot>
    <div class="row gutters-50">
        <div class="col-xl-9 col-lg-8">
            <div class="single-blog-box-layout3">
                <div class="single-blog-content">
                    <h1 class="item-title">当サイトについて</h1>
                    <div class="blog-details">
                    <div class="section-heading-4 heading-dark">
                        <h3 class="item-heading">{{ $serviceName }}とは？</h3>
                    </div>
                        <p>
                            「{{ $serviceName }}」をご覧いただきありがとうございます。　<br>
                            「{{ $serviceName }}」は無料で視聴可能なアダルト動画を紹介するサイトです。<br>
                            18歳未満の方はご利用いただけません。<br>
                        </p>
                    <div class="section-heading-4 heading-dark">
                        <h3 class="item-heading">免責事項</h3>
                    </div>
                        <p>
                            当サイトからリンクやバナーなどによって他のサイトに移動された場合、移動先サイトで提供される情報、サービス等について一切の責任を負いません。<br>
                            動画・画像・音声等の知的所有権も著作者・団体に帰属します。リンク先での動画の削除などの質問に関しては各動画共有サイトへ直接お願いいたします。<br>
                        </p>
                    <div class="section-heading-4 heading-dark">
                        <h3 class="item-heading">不具合やご要望</h3>
                    </div>
                        <p>
                            ご利用中の不具合やご要望、削除依頼などありましたら、<a href="{{ route('contact') }}">お問い合わせページ</a>よりご連絡ください。<br>
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <x-sidebar/>
    </div>
</x-base>