@php

$status = $exception->getStatusCode();

switch ($status) {
        case 400:
            $message = 'Bad Request';
            $detail  = '';
            break;
        case 401:
            $message = 'Unauthorized';
            $detail  = '認証に失敗しました。';
            break;
        case 403:
            $message = 'Forbidden';
            $detail  = 'アクセス権がありません。';
            break;
        case 404:
            $message = 'Not Found';
            $detail  = '存在しないページです。';
            break;
        case 405:
            $message = 'Method Not Allowed';
            $detail  = 'アクセスしようとしたページは表示できませんでした。';
            break;
        case 408:
            $message = 'Request Timeout';
            $detail  = 'タイムアウトです。';
            break;
        case 414:
            $message = 'URI Too Long';
            $detail  = 'リクエストURIが長すぎます。';
            break;
        case 419:
            $message = 'CSRF Token Expired';
            $detail  = 'リクエストの有効期限切れです。';
            break;
        case 500:
            $message = 'Internal Server Error';
            $detail  = 'アクセスしようとしたページは表示できませんでした。';
            break;
        case 503:
            $message = 'Service Unavailable';
            $detail  = 'アクセスしようとしたページは表示できませんでした。';
            break;
        default:
            $message = 'エラー';
            $detail  = 'アクセスしようとしたページは表示できませんでした。';
            break;
}

@endphp

<x-base title="{{ $status }} : {{ $message }}"
        description="{{ $message }}"
>

    <x-slot name="subHeadline"></x-slot>

     <section class="error-page-wrap">
        <div class="container">
            <div class="error-page-box">
                <h2 class="error-title">{{ $status }} : {{ $message }}</h2>
                <p>{{ $detail }}</p>
                <p>大変お手数ですがブラウザの戻るボタンで戻るか、以下のトップページへ戻るボタンよりお進みください。</p>
                <a href="{{ route('home') }}" class="item-btn">トップページへ戻る</a>
            </div>
        </div>
    </section>
</x-base>