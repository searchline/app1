<x-base title="お問い合わせありがとうございます"
        description=""
>
    <x-slot name="subHeadline"></x-slot>
    <div class="row gutters-50">
        <div class="col-xl-9 col-lg-8">
            <div class="single-blog-box-layout3">
                <div class="thanks-box">
                    <h1 class="thanks-box__headline">お問い合わせありがとうございます。</h1>
                    <p class="thanks-box__description">
                        ご入力頂いた情報は無事送信されました。<br>
                        確認のため自動返信メールをお送りさせていただきました。<br>
                        お問い合わせ頂き、ありがとうございました。<br>
                    </p>
                    <a href="{{ route('home') }}" class="item-btn">トップページへ戻る</a>
                </div>
            </div>
        </div>
        <x-sidebar/>
    </div>
</x-base>