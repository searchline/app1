<x-base title="お問い合わせ内容確認"
        description=""
>
    <x-slot name="subHeadline"></x-slot>
    <div class="row gutters-50">
        <div class="col-xl-9 col-lg-8">
            <div class="single-blog-box-layout3">
                <div class="single-blog-content">
                    <div class="blog-form">
                        <div class="section-heading-4 heading-dark">
                            <h1 class="item-heading">お問い合わせ内容確認</h1>
                        </div>
                        <form class="contact-form-box contact-form" method="POST" action="{{ route('contact.send') }}">
                            @csrf
                            <div class="row gutters-15">
                                <div class="col-12 form-group">
                                    <label>お名前</label>
                                    <input readonly class="form-control" name="name" value="{{ $inputs['name'] }}">
                                </div>
                                <div class="col-12 form-group">
                                    <label>E-mail</label>
                                    <input readonly class="form-control" name="email" value="{{ $inputs['email'] }}">
                                </div>
                                <div class="col-12 form-group">
                                    <label>お問い合わせ内容</label>
                                    <textarea readonly class="textarea form-control"  name="message" cols="20" rows="9">{{ $inputs['message'] }}</textarea>
                                </div>
                                <div class="col-6 form-group">
                                    <button class="item-btn" name="action" value="back">入力内容修正</button>
                                </div>
                                <div class="col-6 form-group">
                                    <button class="item-btn" name="action" value="submit">送信する</button>
                                </div>
                            </div>
                            <div class="form-response"></div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <x-sidebar/>
    </div>
</x-base>