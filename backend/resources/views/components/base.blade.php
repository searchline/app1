<!doctype html>
<html class="no-js" lang="ja">

<head>
    @if(app()->isProduction())
    <x-templates.gtm-script/>
    @endif
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>{{ $title }} - {{ config('app.name') }}</title>
    <meta name="description" content="{{ $description }}{{ config('app.name') }}は無料エロ動画紹介サイトです！カテゴリーやタグ、キーワードで無料エロ動画がすぐに検索できます。">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('img/favicon.png') }}">
    <link rel="stylesheet" href="{{ asset('css/common.css') }}">
    <link rel="stylesheet" href="{{ asset('css/main.css') }}">
    <!-- Modernize js -->
    <script src="{{ asset('js/modernizr-3.6.0.min.js') }}"></script>
</head>

<body class="sticky-header">
    @if(app()->isProduction())
    <x-templates.gtm-noscript/>
    @endif
    <!--[if lte IE 9]>
    <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
      <![endif]-->
    <!-- Preloader Start Here -->
    <div id="preloader"></div>
    <!-- Preloader End Here -->
    <div id="wrapper" class="wrapper">

        <!-- header -->
        <x-templates.header/>

        {{ $subHeadline }}

        <!-- Blog Area Start Here -->
        <section class="blog-wrap-layout15 container-xl">
            <div class="container">
                <!-- <div class="row gutters-50"> -->
                    {{ $slot }}
                    <!-- sidebar -->
                <!-- </div> -->
            </div>
        </section>

        <!-- footer & searchbox -->
        <x-templates.footer/>

    </div>

    <!-- js include -->
    <x-templates.js/>

</body>

</html>