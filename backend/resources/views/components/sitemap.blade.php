<?php echo '<?xml version="1.0" encoding="UTF-8"?>'; ?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">

    <url>
        <loc>{{ route('home') }}</loc>
        <lastmod>2021-11-01</lastmod>
        <changefreq>daily</changefreq>
        <priority>1.0</priority>
    </url>

    @foreach ($posts as $post)
        <url>
            <loc>{{ route('single', [ 'id' => $post->id ] ) }}</loc>
            <lastmod>{{ $post->list_date }}</lastmod>
            <changefreq>monthly</changefreq>
            <priority>0.8</priority>
        </url>
    @endforeach

    @foreach ($categories as $category)
        <url>
            <loc>{{ route('category', [ 'id' => $category->id ]) }}</loc>
            <lastmod>{{ $category->updated_at->format('Y-m-d') }}</lastmod>
            <changefreq>daily</changefreq>
            <priority>0.8</priority>
        </url>
    @endforeach

    @foreach ($tags as $tag)
        <url>
            <loc>{{ route('tag', [ 'id' => $tag->id ]) }}</loc>
            <lastmod>{{ substr($tag->updated_at, 0, 10) }}</lastmod>
            <changefreq>daily</changefreq>
            <priority>0.8</priority>
        </url>
    @endforeach

</urlset>