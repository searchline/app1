<x-base title="{{ $headline['sub'] }} : {{ $headline['main'] }}の動画が{{ $posts->total() }}件"
        description="{{ $headline['sub'] }} : {{ $headline['main'] }}の動画が{{ $posts->total() }}件あります。"
>
    <x-slot name="subHeadline">
         <section class="inner-page-banner bg-common">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="breadcrumbs-area">
                            <ul>
                                <h1>{{ $headline['main'] }} : {{ $posts->total() }}件</h1>
                                <li>
                                    <a href="{{ route('home') }}">Home</a>
                                </li>
                                <li>{{ $headline['sub'] }}</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </x-slot>
    <div class="row gutters-50">
        <div class="col-xl-9 col-lg-8">
            <div class="row gutters-40">
                @foreach( $posts as $post )
                <div class="col-md-4 col-12">
                    <div class="blog-box-layout2">
                        <div class="item-img">
                            <a href="{{ route('single', [ 'id' => $post->id ] ) }}"><img src="{{ $post->image_path }}" alt="{{ $post->filename }}"></a>
                        </div>
                        <div class="item-content">
                            <ul class="entry-meta meta-color-dark">
                                <li><i class="fas fa-video"></i>{{ $post->videoSite->name }}</li>
                                <li><i class="fas fa-calendar-alt"></i>{{ $post->list_date }}</li>
                            </ul>
                            <h3 class="item-title"><a href="{{ route('single', [ 'id' => $post->id ] ) }}">{{ $post->title }}</a></h3>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
            {{ $posts->appends(Request::except('page'))->links('components.templates.pagination') }}
        </div>
        <x-sidebar :searchAction="$searchAction" search="{{ $search }}"/>
    </div>
</x-base>