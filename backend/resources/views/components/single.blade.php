<x-base title="{{ $post->title }}"
        description=""
>
    <x-slot name="subHeadline"></x-slot>
    <div class="row gutters-50">
        <div class="col-xl-9 col-lg-8">
            <div class="single-blog-box-layout3">
                <div class="blog-banner">
                    <img src="{{ $post->image_path }}" alt="{{ $post->filename }}">
                </div>
                <div class="single-blog-content">
                    <div class="blog-entry-content">
                        <ul class="entry-meta meta-color-dark">
                            <li><i class="fas fa-video"></i>{{ $post->videoSite->name }}</li>
                            <li><i class="fas fa-calendar-alt"></i>{{ $post->list_date }}</li>
                        </ul>
                        <h1 class="item-title">{{ $post->title }}</h1>

                        @if($actresses)
                        <ul class="tags-area">
                            @foreach( $actresses as $actress )
                            <!-- <li><a href="{{ route('tag', [ 'id' => $actress->id ]) }}"><i class="fas fa-female fa-lg"></i>&nbsp;{{ $actress->name }}</a></li> -->
                            <li class="badge badge-pill badge-primary actress"><a href="{{ route('actress', [ 'id' => $actress->id ]) }}"><i class="fas fa-female fa-lg"></i>&nbsp;{{ $actress->name }}</a></li>
                            @endforeach
                        </ul>
                        @endif

                        <ul class="tags-area">
                            @foreach( $tags as $tag )
                            <li><a href="{{ route('tag', [ 'id' => $tag->id ]) }}"><i class="fas fa-tags"></i>&nbsp;{{ $tag->name }}</a></li>
                            @endforeach
                        </ul>

                        <div class="blog-form">
                            <form class="contact-form-box">
                                <div class="col-12 form-group">
                                    <a href="{{ $post->url }}" class="item-btn" target="_blank" rel="noopener noreferrer">動画を見る</a>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="related-item">
                        <div class="section-heading heading-dark">
                            <h2 class="item-heading">おすすめ動画</h2>
                        </div>
                        <div class="row gutters-40">
                            @foreach( $relatedPosts as $post )
                            <div class="col-md-4 col-12">
                                <div class="blog-box-layout2">
                                    <div class="item-img">
                                        <a href="{{ route('single', [ 'id' => $post->id ] ) }}"><img src="{{ $post->image_path }}" alt="{{ $post->filename }}"></a>
                                    </div>
                                    <div class="item-content">
                                        <ul class="entry-meta meta-color-dark">
                                            <li><i class="fas fa-video"></i>{{ $post->videoSite->name }}</li>
                                            <li><i class="fas fa-calendar-alt"></i>{{ $post->list_date }}</li>
                                        </ul>
                                        <h3 class="item-title"><a href="{{ route('single', [ 'id' => $post->id ] ) }}">{{ $post->title }}</a></h3>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <x-sidebar/>
    </div>
</x-base>