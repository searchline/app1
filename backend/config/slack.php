<?php

return [

    'channel' => env('SLACK_CHANNEL'),

    'webhook_url' => env('SLACK_WEBHOOK_URL'),

];