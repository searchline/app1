<?php

namespace App\View\Components;

use Illuminate\View\Component;
use App\Models\Masters\Post;
use App\Models\Masters\PostView;
use App\Models\Masters\VideoSite;
use App\Models\Masters\Tag;
use Illuminate\Support\Facades\DB;

class Sidebar extends Component
{
    public $categories = [];
    public $tags = [];
    public $popularPosts = [];
    public $searchAction;
    public $search;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct( $searchAction = '/', $search = "" )
    {
        $this->categories = VideoSite::withCount('posts')->orderBy('id')->having('posts_count', '>', '0')->get();

        $this->tags = DB::table('post_tag')->joinSub(Post::orderBy('id'), 'posts', function ($query) {
                                                $query->on('post_tag.post_id', '=', 'posts.id');
                                            })
                                            ->join('tags', 'tags.id', '=', 'post_tag.tag_id')
                                            ->select(DB::raw('count(post_tag.tag_id) as posts_count, tags.id as id, tags.name as name'))
                                            ->groupBy('post_tag.tag_id')
                                            ->orderBy('posts_count', 'desc')->limit(70)->get();

        $postView = PostView::select([DB::raw('post_id, sum(view_count) as count')])->groupBy('post_id');
        $this->popularPosts = Post::joinSub($postView, 'post_views', function ($query) {
                $query->on('posts.id', '=', 'post_views.post_id');
        })->orderBy('count', 'desc')->limit(5)->get();

        if( !$this->popularPosts->count() ) {
            if( Post::all()->count() >= 5){
                $this->popularPosts = Post::all()->random(5);
            } else {
                $this->popularPosts = Post::limit(5)->get();
            }
        }

        $this->searchAction = $searchAction;

        $this->search = $search;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.templates.sidebar');
    }
}
