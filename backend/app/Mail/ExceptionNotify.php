<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Throwable;

class ExceptionNotify extends Mailable
{
    use Queueable, SerializesModels;

    private $error;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct( Throwable $exception, $status )
    {
        $this->error['message'] = $exception->getMessage();
        $this->error['status']  = $status;
        $this->error['code']    = $exception->getCode();
        $this->error['file']    = $exception->getFile();
        $this->error['line']    = $exception->getLine();
        $this->error['url']     = url()->current();
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->text('emails.errors.exception')
                    ->from(config('mail.from.address'))
                    ->subject('【'.config('app.name').'】['.config('app.env').'] サーバーエラー発生の連絡')
                    ->with(['e' => $this->error]);
    }
}
