<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class ViewsCount extends Command
{

    protected $viewsCount;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'Viewscount:run';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '閲覧数カウント DB保存';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        // app('redisViewCount')->saveCount();
    }
}
