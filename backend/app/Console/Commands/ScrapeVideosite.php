<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Storage;
use App\Services\Scraper\Scraper;

class ScrapeVideosite extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'scrape:videosite {site?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Scrape Videosite';


    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $site = $this->argument('site');

        if( is_null($site) ){

            Scraper::runVideoSite();

        } else {

            switch ( $site ){

                case 'xvideos' :
                    Scraper::runXvideos();
                    break;

                case 'pornhub' :
                    Scraper::runPornhub();
                    break;

                case 'javynow' :
                    Scraper::runJavynow();
                    break;

                case 'sharevideos' :
                    Scraper::runSharevideos();
                    break;

                case 'xhamster' :
                    Scraper::runXhamster();
                    break;

                case 'redtube' :
                    Scraper::runRedtube();
                    break;

                case 'youjizz' :
                    Scraper::runYoujizz();
                    break;

                case 'spankingbang' :
                    Scraper::runSpankingbang();
                    break;

                default :
                    break;

            }

        }

    }

}
