<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;
use App\Services\Scraper\Scraper;
use App\Models\Masters\Post;

class UploadImage extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'scrape:uploadimage {video_site_id?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Scrape Upload Image';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $video_site_id = $this->argument('video_site_id');

        if( is_null($video_site_id) ) {

            $posts = Post::where('filename', null)
                     ->where('unpublished', false)
                     ->get();

        } else {

            $posts = Post::where('filename', null)
                     ->where('unpublished', false)
                     ->where('video_site_id', $video_site_id)
                     ->get();
        }

        $bar = $this->output->createProgressBar(count($posts));

        $bar->start();

        foreach ($posts as $post) {

            Scraper::uploadImage($post);

            $bar->advance();

        }

        $bar->finish();

        echo PHP_EOL;
    }
}
