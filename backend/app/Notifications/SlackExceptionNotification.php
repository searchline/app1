<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Messages\SlackMessage;
use Illuminate\Notifications\Notification;
use Throwable;

class SlackExceptionNotification extends Notification
{
    use Queueable;

    protected $channel;
    protected $exception;
    protected $error;
    protected $status;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct( Throwable $exception, $status )
    {
        $this->channel = config('slack.channel');
        $this->status = $status;

        $this->exception = $exception;
        $this->error['message'] = $exception->getMessage();
        $this->error['status']  = $status;
        $this->error['code']    = $exception->getCode();
        $this->error['file']    = $exception->getFile();
        $this->error['line']    = $exception->getLine();
        $this->error['url']     = url()->current();
        $this->error['action']  = (\Route::getCurrentRoute()) ? \Route::getCurrentRoute()->getActionName() : "n/a";
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['slack'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toSlack($notifiable)
    {
        return (new SlackMessage)
                    ->from(config('app.name').'通知')
                    ->to($this->channel)
                    ->error()
                    ->content('エラーを検知しました')
                    ->attachment(function ($attachment) {
                        $attachment
                            ->title(get_class($this->exception))
                            ->content(
                                $this->exception->getMessage()."\n".
                                '[Message]'."\n".
                                $this->error['message']."\n".
                                '[Action] '.$this->error['action']."\n".
                                '[URL] '.$this->error['url']."\n".
                                '[File] '.$this->error['file']."\n".
                                '[Line] '.$this->error['line']."\n".
                                '[Code] '.$this->error['code']."\n".
                                '[Status] '.$this->error['status']
                            );
                    });
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
