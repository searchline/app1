<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class ScrapingServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'scrapingService',
            'App\Services\Scraper\ScrapingService'
        );

        // id:1
        $this->app->bind(
            'xvideosScraper',
            'App\Services\Scraper\XvideosScraper'
        );

        // id:2
        $this->app->bind(
            'pornhubScraper',
            'App\Services\Scraper\PornhubScraper'
        );

        // id:3
         $this->app->bind(
            'javynowScraper',
            'App\Services\Scraper\JavyNowScraper'
        );

        // id:4
        $this->app->bind(
            'sharevideosScraper',
            'App\Services\Scraper\ShareVideosScraper'
        );

        // id:5
        $this->app->bind(
            'xhamsterScraper',
            'App\Services\Scraper\XhamsterScraper'
        );

        // id:6
        $this->app->bind(
            'redtubeScraper',
            'App\Services\Scraper\RedTubeScraper'
        );

         // id:7
         $this->app->bind(
            'youjizzScraper',
            'App\Services\Scraper\YouJizzScraper'
        );

        // id:8
        $this->app->bind(
            'spankingbangScraper',
            'App\Services\Scraper\SpankingbangScraper'
        );
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
