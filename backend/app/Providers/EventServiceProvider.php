<?php

namespace App\Providers;

use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;

use Illuminate\Routing\Events\RouteMatched;
use Illuminate\Support\Str;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        'App\Events\ContactSent' => [
            'App\Listeners\Contact\SendReplyEmail',
            'App\Listeners\Contact\SendNotifySlackToAdmin'
            // 'App\Listeners\Contact\SendNotifyEmailToAdmin'
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        Event::listen(RouteMatched::class, function (RouteMatched $event) {
            if (Str::startsWith($event->route->uri, 'admin/')) {
                app('scopeManager')->setActive(false);
                // \Debugbar::info(app('scopeManager')->getActive());
            } else {
                app('scopeManager')->setActive(true);
                // \Debugbar::info(app('scopeManager')->getActive());
            }
        });
    }
}
