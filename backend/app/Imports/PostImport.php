<?php

namespace App\Imports;

use App\Models\Masters\Post;
use Maatwebsite\Excel\Concerns\Importable;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Illuminate\Support\Str;
use Carbon\Carbon;

class PostImport implements ToCollection
{
    use Importable;

    public function collection(Collection $rows)
    {
        $posts = [];
        $created_at = Carbon::now();

        foreach ($rows as $index => $row)
        {
            if($index == 0) continue;
            $posts[] = [
                'id' => (string) Str::uuid(),
                'created_at' => $created_at,
                'url' => $row[0],
                'thumbnail' => $row[1],
                'time' => $this->timeFormat($row[2]),
                'video_site_id' => $row[3],
            ];

            if( count($posts) >= 1000 ) {
                Post::upsert($posts, ['url'], []);
                $posts = [];
            }
        }

        Post::upsert($posts, ['url'], []);
    }

    public function timeFormat($time)
    {
        $time = (int) $time;

        if( $time >= 60 ) {
            $minutes  = floor($time/60);
            $seconds = $time%60;

            $minutes  = str_pad($minutes, strlen($minutes), 0, STR_PAD_LEFT);
            $seconds = str_pad($seconds, 2, 0, STR_PAD_LEFT);

            $adjTime = $minutes.':'.$seconds;
        } else {
            $seconds = $time;

            $seconds = str_pad($seconds, 2, 0, STR_PAD_LEFT);
            $adjTime = '00:'.$seconds;
        }

        return $adjTime;
    }
}
