<?php

namespace App\Imports;

use App\Models\Masters\Actress;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\WithChunkReading;

class ActressImport implements ToModel, WithChunkReading
{
    use Importable;

    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        // return new Actress([
        //     'name' => $row[1]
        // ]);

        if(!is_null($row[1])){
            Actress::updateOrCreate([
                'name' => $row[1]
            ]);
        }

    }

    public function chunkSize(): int
    {
        return 500;
    }
}
