<?php

namespace App\Imports;

use App\Models\Masters\Tag;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\WithChunkReading;

class TagImport implements ToModel
{
    use Importable;

    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        if(!is_null($row[1])){
            Tag::updateOrCreate([
                'name' => $row[1]
            ]);
        }
    }

    public function chunkSize(): int
    {
        return 500;
    }
}
