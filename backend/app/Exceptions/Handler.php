<?php

namespace App\Exceptions;

use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Throwable;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use App\Mail\ExceptionNotify;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        \Illuminate\Auth\AuthenticationException::class,
        \Illuminate\Auth\Access\AuthorizationException::class,
        \Illuminate\Validation\ValidationException::class,
    ];

    public function report(Throwable $exception)
    {
        $status = $this->isHttpException($exception) ? $exception->getStatusCode() : 500;

        if ($exception instanceof \Throwable && $this->shouldReport($exception))
        {
            if ( app()->isProduction() || app()->runningUnitTests() )
            {
                if ($status >= 500)
                {

                    // \Mail::to(config('mail.admin'))->send(new ExceptionNotify($exception, $status));
                    \Slack::sendException($exception, $status);

                }
            }
        }
        parent::report($exception);
    }

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->reportable(function (Throwable $e) {
            //
        });
    }

    /**
     * Render the given HttpException.
     *
     * @param  \Symfony\Component\HttpKernel\Exception\HttpExceptionInterface  $e
     * @return \Symfony\Component\HttpFoundation\Response
     */
    protected function renderHttpException(HttpExceptionInterface $e)
    {
        $this->registerErrorViewPaths();

        return response()->view('components.errors', [
                    'exception' => $e,
                ], $e->getStatusCode(), $e->getHeaders());
    }
}
