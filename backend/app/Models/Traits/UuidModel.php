<?php

namespace App\Models\Traits;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;


trait UuidModel
{
    protected static function bootUuidModel()
    {
        // レコード作成時にprimary keyに自動的にuuidを入れてくれるようにする
        static::creating(function ($model) {
            $model->{$model->getKeyName()} = (string) Str::uuid();
        });
    }

    /**
     * Get the value indicating whether the IDs are incrementing.
     * Modelクラスの自動連番をオーバーライド
     *
     * @return bool
     */
    public function getIncrementing()
    {
        return false;
    }

    /**
     * Set whether IDs are incrementing.
     * Modelクラスのプライマリーキーの型をオーバーライド
     *
     * @param  bool  $value
     * @return $this
     */
    public function getKeyType()
    {
        return 'string';
    }
}