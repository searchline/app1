<?php

namespace App\Models\Masters;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Encore\Admin\Traits\DefaultDatetimeFormat;
use App\Scopes\PublishedPostScope;
use App\Models\Traits\UuidModel;
use Illuminate\Support\Facades\Storage;

class Post extends Model
{
    use HasFactory;
    use DefaultDatetimeFormat;
    use UuidModel;

    protected $guarded = ['id'];
    protected $dates = ['release_date'];

    function videoSite ()
    {
        return $this->belongsTo(VideoSite::class);
    }

    function tags ()
    {
        return $this->belongsToMany(Tag::class);
    }

    function actresses ()
    {
        return $this->belongsToMany(Actress::class);
    }

    function getPostHistory ()
    {
        return $this->belongsTo(\App\Models\Histories\GetPostHistory::class);
    }

    function postViews ()
    {
        return $this->hasMany(PostView::class);
    }

    public function scopeKeyWordSearch($query, $search)
    {
        return $query->where('title', 'like', '%'. $search .'%')

        ->orWhereHas('tags', function ($query) use ($search) {
            $query->where('tags.name', 'like', '%'. $search .'%');
        })
        ->orWhereHas('actresses', function ($query) use ($search) {
            $query->where('actresses.name', 'like', '%'. $search .'%');
        });
    }

    public function getImagePathAttribute()
    {
        return config('app.url').'/images/'.$this->filename;
    }

    public function getListDateAttribute(): string
    {
        return $this->release_date ? $this->release_date->format('Y-m-d') : $this->updated_at->format('Y-m-d');
    }

    public function getViewCountAttribute()
    {
        return $this->postViews() ? $this->postViews()->sum('view_count') : 0;
    }

    protected static function booted()
    {
        if( app('scopeManager')->getActive() ) {
            static::addGlobalScope(new PublishedPostScope);
        }
    }

    public static function boot()
    {
        parent::boot();

        static::deleting(function ($post) {

            $post->tags()->detach();
            $post->actresses()->detach();
            $post->postViews()->delete();

            Storage::disk('images')->delete( $post->filename );

        });
    }

}
