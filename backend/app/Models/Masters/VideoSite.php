<?php

namespace App\Models\Masters;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Encore\Admin\Traits\DefaultDatetimeFormat;

class VideoSite extends Model
{
    use HasFactory;
    use DefaultDatetimeFormat;

    protected $guarded = ['id'];

    function posts ()
    {
        return $this->hasMany(Post::class);
    }
}
