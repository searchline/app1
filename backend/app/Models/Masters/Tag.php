<?php

namespace App\Models\Masters;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Encore\Admin\Traits\DefaultDatetimeFormat;

class Tag extends Model
{
    use HasFactory;
    use DefaultDatetimeFormat;

    protected $guarded = ['id'];

    function posts ()
    {
        return $this->belongsToMany(Post::class);
    }

}
