<?php

namespace App\Models\Masters;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PostView extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    function post ()
    {
        return $this->belongsTo(Post::class);
    }

}
