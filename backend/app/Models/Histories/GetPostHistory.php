<?php

namespace App\Models\Histories;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class GetPostHistory extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    function posts ()
    {
        return $this->hasMany(\App\Models\Masters\Post::class);
    }

    function VideoSite ()
    {
        return $this->belongsTo(\App\Models\Masters\VideoSite::class);
    }
}
