<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AboutController extends Controller
{
    public function index()
    {
        $serviceName = config('app.name');
        return view('components.about', compact('serviceName'));
    }
}
