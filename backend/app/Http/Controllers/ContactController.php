<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Events\ContactSent;

class ContactController extends Controller
{

    public function index()
    {
        return view('components.contact');
    }

    public function confirm(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required|email',
            'message'  => 'required',
        ],
        [
            'name.required' => 'お名前は必須です。',
            'email.required'  => 'E-mailは必須項目です。',
            'message.required'  => 'お問い合わせ内容は必須項目です。',
        ]);

        $inputs = $request->all();

        return view('components.confirm', [
            'inputs' => $inputs,
        ]);
    }

    public function send(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required|email',
            'message'  => 'required',
        ],
        [
            'name.required' => 'お名前は必須です。',
            'email.required'  => 'E-mailは必須項目です。',
            'message.required'  => 'お問い合わせ内容は必須項目です。',
        ]);

        $inputs = $request->except('action');

        $action = $request->input('action');

        if( $action !== 'submit' ){
            return redirect()->route('contact')->withInput($inputs);
        } else {
            $request->session()->regenerateToken();

            event(new ContactSent($inputs));

            return view('components.thanks');
        }
    }
}
