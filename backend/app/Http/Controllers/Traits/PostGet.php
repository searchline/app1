<?php

namespace App\Http\Controllers\Traits;
use App\Models\Masters\Post;

trait PostGet {

    protected $post;

    protected $maxPage = 30;

    public function __construct( Post $post )
    {
        $this->post = $post;
    }
}