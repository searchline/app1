<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Masters\Post;
use App\Models\Masters\Actress;
use App\Http\Controllers\Traits\PostGet;

class ActressController extends Controller
{
    use PostGet;

    public function archive($id, Request $request)
    {
        $search = $request->input('search');

        $actress = Actress::find($id);

        if( is_null($actress) ) return Abort('404');

        $headline =[
            'main' => $actress->name,
            'sub'  => '女優',
        ];

        $searchAction = route('actress', [ 'id' => $id ]);

        $query = $this->post::query()->whereHas( 'actresses', function ( $query ) use ( $id ) {
            $query->where('actresses.id', $id);
        })
        ->where(function ($query) use ($search) {

            if( !empty($search) ){
                $query->KeyWordSearch($search);
            }

        });

        $posts = $query->paginate($this->maxPage);

        // ページ数以上最終ページ
        if( request('page') > $posts->lastPage() ){
            return redirect()->route('actress', [ 'id' => $id, 'page' => $posts->lastPage() ]);
        }
        return view('components.section', compact('posts', 'headline', 'searchAction', 'search'));
    }
}
