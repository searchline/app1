<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Masters\Post;
use App\Models\Masters\VideoSite;
use App\Models\Masters\Tag;
use DB;

class SitemapController extends Controller
{
    private $post;

    public function __construct( Post $post)
    {
        $this->post = $post;
    }
    public function index()
    {
        $posts = $this->post::all();

        $categories = VideoSite::withCount('posts')->having('posts_count', '>', '0')->orderBy('id')->get();

        $tags = DB::table('post_tag')->joinSub(Post::orderBy('id'), 'posts', function ($query) {
                                        $query->on('post_tag.post_id', '=', 'posts.id');
                                    })
                                    ->join('tags', 'tags.id', '=', 'post_tag.tag_id')
                                    ->select(DB::raw('count(post_tag.tag_id) as posts_count, tags.id as id, tags.name as name, tags.updated_at as updated_at'))
                                    ->groupBy('post_tag.tag_id')
                                    ->orderBy('tags.id')->get();

        return response()->view('components.sitemap', [
            'posts' => $posts,
            'categories' => $categories,
            'tags' => $tags,
        ])->header('Content-Type', 'text/xml');
    }
}
