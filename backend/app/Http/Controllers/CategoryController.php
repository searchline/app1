<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Masters\Post;
use App\Models\Masters\VideoSite;
use App\Http\Controllers\Traits\PostGet;

class CategoryController extends Controller
{
    use PostGet;

    public function archive($id, Request $request)
    {
        $search = $request->input('search');

        $videoSite = VideoSite::find($id);

        if( is_null($videoSite) ) return Abort('404');

        $headline =[
            'main' => $videoSite->name,
            'sub'  => 'カテゴリー',
        ];

        $searchAction = route('category', [ 'id' => $id ]);

        $query = $this->post::query()->where('video_site_id', $id)
        ->where(function ($query) use ($search) {

            if( !empty($search) ){
                $query->KeyWordSearch($search);
            }

        });

        $posts = $query->paginate($this->maxPage);

        // ページ数以上最終ページ
        if( request('page') > $posts->lastPage() ){
            return redirect()->route('category', [ 'id' => $id, 'page' => $posts->lastPage() ]);
        }
        return view('components.section', compact('posts', 'headline', 'searchAction', 'search'));
    }
}
