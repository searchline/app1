<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Masters\Post;
use App\Models\Masters\VideoSite;
use App\Models\Masters\Tag;
use App\Models\Masters\Actress;
use App\Http\Controllers\Traits\PostGet;

class HomeController extends Controller
{
    use PostGet;

    public function index(Request $request)
    {
        $search = $request->input('search');

        $query = $this->post::query();

        if( !empty($search) ){
            $query->KeyWordSearch($search);
        }

        $posts = $query->paginate($this->maxPage);

        if($search) {
            $homeTitle = 'キーワード : '.$search.'の動画が'.$posts->total().'件';
            $description = 'キーワード : '.$search.'の動画が'.$posts->total().'件あります。';
        }
        // ページ数以上最終ページ
        if( request('page') > $posts->lastPage() ){
            return redirect()->route('home', [ 'page' => $posts->lastPage() ]);
        }
        return view('components.home', compact('posts', 'search'));
    }

    public function view(Request $request, $id)
    {
        $post = $this->post::find($id);

        $tags = $post->tags;

        $actresses = $post->actresses;

        $tagsId = $post->tags->pluck('id');

        $relatedPosts = $this->post::whereHas('tags', function ($query) use ($tagsId) {
            $query->whereIn('tags.id', $tagsId);
        })->inRandomOrder()->limit(15)->get();

        if( empty($post) ) return abort(404);

        app('viewCount')->addCount( $post, $request->ip() );

        return view('components.single', compact('post', 'actresses', 'tags', 'relatedPosts'));
    }

}
