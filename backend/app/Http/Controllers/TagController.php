<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Masters\Post;
use App\Models\Masters\Tag;
use App\Http\Controllers\Traits\PostGet;

class TagController extends Controller
{
    use PostGet;

    public function archive($id, Request $request)
    {
        $search = $request->input('search');

        $tag = Tag::find($id);

        if( is_null($tag) ) return Abort('404');

        $headline =[
            'main' => $tag->name,
            'sub'  => 'タグ',
        ];

        $searchAction = route('tag', [ 'id' => $id ]);

        $query = $this->post::query()->whereHas( 'tags', function ( $query ) use ( $id ) {
            $query->where('tags.id', $id);
        })
        ->where(function ($query) use ($search) {

            if( !empty($search) ){
                $query->KeyWordSearch($search);
            }

        });

        $posts = $query->paginate($this->maxPage);

        // ページ数以上最終ページ
        if( request('page') > $posts->lastPage() ){
            return redirect()->route('tag', [ 'id' => $id, 'page' => $posts->lastPage() ]);
        }
        return view('components.section', compact('posts', 'headline', 'searchAction', 'search'));
    }

}
