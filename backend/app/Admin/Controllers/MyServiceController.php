<?php

namespace App\Admin\Controllers;

use App\Models\Masters\MyService;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class MyServiceController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'MyService';

    protected $columnName = [
        'id' => 'ID',
        'name' => '名称',
        'created_at' => '登録日時',
        'updated_at' => '更新日時',
    ];

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new MyService());

        $grid->quickCreate(function (Grid\Tools\QuickCreate $create) {
            $create->text('name', 'Name');
        });

        $grid->column('id', $this->columnName['id']);
        $grid->column('name', $this->columnName['name']);
        $grid->column('created_at', $this->columnName['created_at']);
        $grid->column('updated_at', $this->columnName['updated_at']);

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(MyService::findOrFail($id));

        $show->field('id', $this->columnName['id']);
        $show->field('name', $this->columnName['name']);
        $show->field('created_at', $this->columnName['created_at']);
        $show->field('updated_at', $this->columnName['updated_at']);

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new MyService());

        $form->text('name', $this->columnName['name']);

        return $form;
    }
}
