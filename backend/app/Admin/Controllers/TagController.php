<?php

namespace App\Admin\Controllers;

use App\Models\Masters\Tag;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;
use App\Admin\Extensions\Tools\ImportButton;
use Illuminate\Http\Request;
use App\Imports\TagImport;

class TagController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Tag';

    protected $columnName = [
        'id' => 'ID',
        'name' => '名称',
        'created_at' => '登録日時',
        'updated_at' => '更新日時',
    ];

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Tag());

        $grid->quickCreate(function (Grid\Tools\QuickCreate $create) {
            $create->text('name', 'Name');
        });

        $grid->column('id', $this->columnName['id']);
        $grid->column('name', $this->columnName['name']);
        $grid->column('created_at', $this->columnName['created_at']);
        $grid->column('updated_at', $this->columnName['updated_at']);

        $grid->tools(function ($tools) {
            $tools->append(new ImportButton('tags'));
        });

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Tag::findOrFail($id));

        $show->field('id', $this->columnName['id']);
        $show->field('name', $this->columnName['name']);
        $show->field('created_at', $this->columnName['created_at']);
        $show->field('updated_at', $this->columnName['updated_at']);

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Tag());

        $form->text('name', $this->columnName['name']);

        return $form;
    }

    protected function import(Request $request)
    {
        $file = $request->file('file');
        (new TagImport)->import($file);
    }
}
