<?php

namespace App\Admin\Controllers;

use App\Models\Masters\VideoSite;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class VideoSiteController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'VideoSite';

    protected $columnName = [
        'id' => 'ID',
        'name' => '名称',
        'scrape' => '取得',
        'created_at' => '登録日時',
        'updated_at' => '更新日時',
    ];

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new VideoSite());

        $grid->quickCreate(function (Grid\Tools\QuickCreate $create) {
            $create->text('name', 'Name');
        });

        $grid->column('id', $this->columnName['id']);
        $grid->column('name', $this->columnName['name']);
        $grid->column('scrape', $this->columnName['scrape'])->switch();
        $grid->column('created_at', $this->columnName['created_at']);
        $grid->column('updated_at', $this->columnName['updated_at']);

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(VideoSite::findOrFail($id));

        $show->field('id', $this->columnName['id']);
        $show->field('name', $this->columnName['name']);
        $show->check($this->columnName['scrape'])->using([ true => 'ON', false => 'OFF']);
        $show->field('created_at', $this->columnName['created_at']);
        $show->field('updated_at', $this->columnName['updated_at']);

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new VideoSite());

        $form->text('name', $this->columnName['name']);
        $form->switch('scrape', $this->columnName['scrape']);

        return $form;
    }
}
