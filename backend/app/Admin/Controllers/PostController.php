<?php

namespace App\Admin\Controllers;

use App\Models\Masters\Post;
use App\Models\Masters\Tag;
use App\Models\Masters\Actress;
use App\Models\Masters\VideoSite;
use App\Admin\Actions\BatchPostCheckOn;
use App\Admin\Actions\BatchPostCheckOff;
use App\Admin\Actions\BatchPostRelease;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;
Use Encore\Admin\Widgets\Table;
use Carbon\Carbon;
use App\Admin\Extensions\Tools\ImportButton;
use Illuminate\Http\Request;
use App\Imports\PostImport;
use Illuminate\Support\Facades\Storage;

class PostController extends AdminController
{

    /*
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Post';

    protected $columnName = [
        'id' => 'ID',
        'title' => 'TITLE',
        'release_date' => '公開日時',
        'url' => 'URL',
        'time' => '時間',
        'check' => '確認',
        'unpublished' => '公開停止',
        'filename' => 'ファイルネーム',
        'thumbnail' => 'サムネイル',
        'videoSite' => 'VIDEO SITE',
        'tags' => 'タグ',
        'created_at' => '登録日時',
        'updated_at' => '更新日時',
    ];

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Post());

        $grid->model()->orderBy('created_at', 'asc');

        // $grid->disableCreateButton();

        // $grid->id($this->columnName['id'])->sortable();

        $grid->column('url', $this->columnName['url'])->link();
        $grid->column('time', $this->columnName['time'])->editable();
        $grid->column('title', $this->columnName['title'])->editable();
        $grid->column('release_date', $this->columnName['release_date'])->editable('datetime')->sortable();
        $grid->column('check', $this->columnName['check'])->switch([ 'on' => ['text' => '完'], 'off' => ['text' => '未'] ]);
        $grid->column('filename', $this->columnName['filename'])->image()->sortable();
        $grid->column('videoSite.name', $this->columnName['videoSite']);

        $grid->tags($this->columnName['tags'])->display(function ($tags) {
            $tags = array_map(function ($tag) {
                return "<span class='badge badge-secondary'>{$tag['name']}</span>";
            }, $tags);

            return join('&nbsp;', $tags);
        });

        $grid->column('unpublished', $this->columnName['unpublished'])->switch();

        $grid->column('created_at', $this->columnName['created_at'])->sortable();
        $grid->column('updated_at', $this->columnName['updated_at']);

        $grid->actions(function ($actions) {
            // $actions->disableDelete();
        });


        /*
        * バッチアクション
        */
        $grid->batchActions(function ($batch) {
            $batch->add(new BatchPostCheckOff());
        });
        $grid->batchActions(function ($batch) {
            $batch->add(new BatchPostCheckOn());
        });

        $grid->tools(function (Grid\Tools $tools) {
            $tools->append(new BatchPostRelease());
        });

        $grid->tools(function ($tools) {
            $tools->append(new ImportButton('posts'));
        });

        /*
        * フィルター
        */
        $grid->expandFilter();

        $grid->filter(function($filter){

            $filter->scope('release', '公開済')
            ->where('check', true)
            ->where('unpublished', false)
            ->whereNotNull('title')
            ->whereNotNull('filename')
            ->where('release_date', '<=', Carbon::now());

            $filter->scope('beforeRelease', '公開前')
            ->where('check', true)
            ->where('unpublished', false)
            ->whereNotNull('title')
            ->whereNotNull('filename')
            ->where(function ($query) {
                $query->where('release_date', '>', Carbon::now())
                      ->orWhereNull('release_date');
            });

            $filter->scope('title', 'タイトル未入力')
            ->where('check', true)
            ->where('unpublished', false)
            ->whereNull('release_date')
            ->whereNull('title')
            ->whereNotNull('filename');

            $filter->scope('includedImage', '画像有')
            ->whereNotNull('filename');

            $filter->scope('notIncludedImage', '画像無')
            ->whereNull('filename');

            $filter->disableIdFilter();

            $filter->column(1/2, function ($filter) {

                $filter->equal('check', '確認')->radio([
                    '' => '全て',
                    1 => '完了',
                    0 => '未済',
                ]);

                $filter->equal('unpublished', '公開停止')->radio([
                    '' => '全て',
                    1 => 'ON',
                    0 => 'OFF',
                ]);


            });

            $filter->column(1/2, function ($filter) {

                $filter->equal('video_site_id', 'ビデオサイト')->select(VideoSite::all()->pluck('name', 'id'));

                $filter->like('url', 'URL');

                $filter->between('release_date', '公開日時')->datetime();

            });
        });


        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Post::findOrFail($id));

        $show->field('id', $this->columnName['id']);
        $show->field('videoSite.name', $this->columnName['videoSite']);
        $show->field('url',$this->columnName['url']);
        $show->field('time', $this->columnName['time']);
        $show->check($this->columnName['check'])->using([ true => '完', false => '未']);
        $show->check($this->columnName['unpublished']);
        $show->filename($this->columnName['filename'])->image();

        $show->tags('タグ', function ($titles) {
            $titles->disableActions();
            $titles->disableCreateButton();

            $titles->resource('/admin/tags');

            $titles->name('名称')->badge('secondary');

            $titles->filter(function ($filter) {
                $filter->like('名称');
            });
        });

        // $show->tags('女優', function ($titles) {
        //     $titles->disableActions();
        //     $titles->disableCreateButton();

        //     $titles->resource('/admin/actresses');

        //     $titles->name('名称')->badge('secondary');

        //     $titles->filter(function ($filter) {
        //         $filter->like('名称');
        //     });
        // });

        $show->field('created_at', $this->columnName['created_at']);
        $show->field('updated_at', $this->columnName['updated_at']);
        $show->panel()->tools(function ($tools) {
            // $tools->disableDelete();
        });;

            return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Post());

        $form->select('video_site_id', $this->columnName['videoSite'])->options(VideoSite::all()->pluck('name', 'id'))->readonly();
        $form->switch('unpublished', $this->columnName['unpublished']);
        $form->url('url', $this->columnName['url'])->readonly();
        $form->url('thumbnail', $this->columnName['thumbnail'])->readonly();

        $form->text('time', $this->columnName['time']);
        $form->switch('check', $this->columnName['check'])->states([ 'on' => ['text' => '完'], 'off' => ['text' => '未'] ]);
        $form->image('filename', $this->columnName['filename'])->removable();
        $form->text('title', $this->columnName['title']);
        $form->datetime('release_date', $this->columnName['release_date']);

        $form->multipleSelect('tags', 'タグ')->options(Tag::all()->pluck('name', 'id'));
        $form->multipleSelect('actresses', '女優')->options(function ($ids) {

            $actress = Actress::find($ids);

            if ($actress) {
                return $actress->pluck('name', 'id');
            }
        })->ajax('/admin/api/actresses');

        $form->tools(function (Form\Tools $tools) {
            // $tools->disableDelete();
        });

        return $form;
    }

    protected function import(Request $request)
    {
        $file = $request->file('file');

        (new PostImport)->import($file);

    }

}
