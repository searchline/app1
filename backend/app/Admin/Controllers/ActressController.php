<?php

namespace App\Admin\Controllers;

use App\Models\Masters\Actress;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;
use App\Admin\Extensions\Tools\ImportButton;
use Illuminate\Http\Request;
use App\Imports\ActressImport;

class ActressController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Actress';

    protected $columnName = [
        'id' => 'ID',
        'name' => '名称',
        'height' => '身長',
        'bust' => 'バスト',
        'cup' => 'カップ',
        'waist' => 'ウエスト',
        'hip' => 'ヒップ',
        'created_at' => '登録日時',
        'updated_at' => '更新日時',
    ];

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Actress());

        $grid->column('id', $this->columnName['id']);
        $grid->column('name', $this->columnName['name']);
        $grid->column('height', $this->columnName['height']);
        $grid->column('bust', $this->columnName['bust']);
        $grid->column('cup', $this->columnName['cup']);
        $grid->column('waist', $this->columnName['waist']);
        $grid->column('hip', $this->columnName['hip']);
        $grid->column('created_at', $this->columnName['created_at']);
        $grid->column('updated_at', $this->columnName['updated_at']);

        $grid->tools(function ($tools) {
            $tools->append(new ImportButton('actresses'));
        });

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Actress::findOrFail($id));

        $show->field('id', $this->columnName['id']);
        $show->field('name', $this->columnName['name']);
        $show->field('height', $this->columnName['height']);
        $show->field('bust', $this->columnName['bust']);
        $show->field('cup', $this->columnName['cup']);
        $show->field('waist', $this->columnName['waist']);
        $show->field('hip', $this->columnName['hip']);
        $show->field('created_at', $this->columnName['created_at']);
        $show->field('updated_at', $this->columnName['updated_at']);

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Actress());

        $form->text('name', $this->columnName['name']);
        $form->number('height', $this->columnName['height']);
        $form->number('bust', $this->columnName['bust']);
        $form->number('cup', $this->columnName['cup']);
        $form->number('waist', $this->columnName['waist']);
        $form->number('hip', $this->columnName['hip']);

        return $form;
    }

    protected function import(Request $request)
    {
        $file = $request->file('file');
        (new ActressImport)->import($file);
    }

    public function actresses(Request $request)
    {
        $q = $request->get('q');

        // \Debugbar::info(Actress::where('name', 'like', "%$q%")->paginate(null, ['id', 'name as text']));

        return Actress::where('name', 'like', "%$q%")->paginate(null, ['id', 'name as text']);
    }

}
