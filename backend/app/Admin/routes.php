<?php

use Illuminate\Routing\Router;

Admin::routes();

Route::group([
    'prefix'        => config('admin.route.prefix'),
    'namespace'     => config('admin.route.namespace'),
    'middleware'    => config('admin.route.middleware'),
    'as'            => config('admin.route.prefix') . '.',
], function (Router $router) {

    $router->get('/', 'HomeController@index')->name('home');

    $router->resource( 'posts',      PostController::class );
    $router->post( 'posts/import', 'PostController@import');
    $router->resource( 'tags',       TagController::class );
    $router->post( 'tags/import', 'TagController@import');
    $router->resource( 'actresses',  ActressController::class );
    $router->post( 'actresses/import', 'ActressController@import');
    $router->get( 'api/actresses', 'ActressController@actresses');
    $router->resource( 'videosites', VideoSiteController::class );
    $router->resource( 'myservices', MyServiceController::class );

});
