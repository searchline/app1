<?php

namespace App\Admin\Extensions\Tools;

use Encore\Admin\Admin;
use Encore\Admin\Grid\Tools\AbstractTool;
use Illuminate\Support\Facades\Request;

class ImportButton extends AbstractTool
{
    private $path;

    public function __construct(String $path)
    {
        $this->path = $path;

    }

    protected function script()
    {
        return <<<EOT
$('#import-file').on('change', function () {
    var isConfirmed = confirm("Is this OK?");
    if (!isConfirmed) {
        return;
    }

    var formData = new FormData();
    formData.append("file", $("#import-file").prop("files")[0]);
    formData.append("_token", LA.token);

    // CSVファイルをアップロードする
    $.ajax({
        method: "POST",
        url: "/admin/$this->path/import",
        async: true,
        data: formData,
        processData: false,
        contentType: false,
        // success: function (response) {
        //     console.log(response);
        //     $.pjax.reload("#pjax-container");
        //     toastr.success('Upload Successful');
        // }
        // Promiseコールバックを使用
    }).done((response) => {
        $.pjax.reload("#pjax-container");
        toastr.success('Upload Successful');
    }).fail((error) => {
        // console.log('Error ' + error.status + ':' + error.statusText);
        toastr.error('Error ' + error.status + ':' + error.statusText);
    })
});
EOT;
    }

    public function render()
    {
        Admin::script($this->script());

        return view('admin.tools.import_button');
    }
}