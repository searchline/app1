<?php

namespace App\Admin\Actions;

use Encore\Admin\Actions\BatchAction;
use Illuminate\Database\Eloquent\Collection;

class BatchPostCheckOff extends BatchAction
{
    public $name = '確認未済';

    public function handle(Collection $collection)
    {
        $count = 0;

        foreach ($collection as $post) {
            $post->check = false;
            $post->update();
            $count++;
        }

        return $this->response()->success($count.'件更新完了しました！')->refresh();
    }

}