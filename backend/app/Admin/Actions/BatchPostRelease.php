<?php

namespace App\Admin\Actions;

// use App\Models\Masters\MyService;
use Encore\Admin\Actions\BatchAction;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Carbon\Carbon;

class BatchPostRelease extends BatchAction
{
    public $name = '記事公開';
    protected $selector = '.release-posts';

    public function handle(Collection $collection, Request $request)
    {
        $count = 0;

        $release_date = $request->input('release_date');
        $update       = $request->input('update');

        foreach ($collection as $post) {

            if( $update ) {

                $post->release_date = $release_date;
                $post->update();
                $count++;

            } else {

                if(empty($post->release_date)) {

                    $post->release_date = $release_date;
                    $post->update();
                    $count++;

                }

            }

        }

        if( $count == 0 )  {
            return $this->response();
        } else {
            return $this->response()->success($count.'件更新完了しました！')->refresh();
        }

    }

    public function form()
    {
        $this->datetime('release_date', '公開日時')->required();
        $this->select('update', '更新区分')->options([ 0 => '上書きしない', 1 => '上書きする']);
    }

    public function html()
    {
    }

}