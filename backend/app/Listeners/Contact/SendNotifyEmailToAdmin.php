<?php

namespace App\Listeners\Contact;

use App\Events\ContactSent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use App\Mail\ContactNotify;

class SendNotifyEmailToAdmin
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ContactSent  $event
     * @return void
     */
    public function handle(ContactSent $event)
    {
        \Mail::to(config('mail.admin'))->send(new ContactNotify($event->inputs));
    }
}
