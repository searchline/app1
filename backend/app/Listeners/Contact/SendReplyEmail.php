<?php

namespace App\Listeners\Contact;

use App\Events\ContactSent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use App\Mail\ContactReply;

class SendReplyEmail
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ContactSent  $event
     * @return void
     */
    public function handle(ContactSent $event)
    {
        \Mail::to($event->inputs['email'])->send(new ContactReply($event->inputs));
    }
}
