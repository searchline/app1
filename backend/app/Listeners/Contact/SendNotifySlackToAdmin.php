<?php

namespace App\Listeners\Contact;

use App\Events\ContactSent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class SendNotifySlackToAdmin
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ContactSent  $event
     * @return void
     */
    public function handle(ContactSent $event)
    {
        \Slack::send(
        "==============================="."\n".
        "お問い合わせが届きました。"."\n".
        "==============================="."\n".
        "\n".
        "お名前：".$event->inputs['name']."\n".
        "email：".$event->inputs['email']."\n".
        "お問い合わせ内容："."\n".
        $event->inputs['message']."\n".
        "\n"
        );
    }
}
