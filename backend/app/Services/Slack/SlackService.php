<?php

namespace App\Services\Slack;

use Illuminate\Notifications\Notifiable;
use App\Notifications\SlackNotification;
use App\Notifications\SlackExceptionNotification;

class SlackService
{
    use Notifiable;

    public function send($message = null)
    {
    $this->notify(new SlackNotification($message));
    }

    public function sendException( $exception, $status )
    {
    $this->notify(new SlackExceptionNotification( $exception, $status ));
    }

    protected function routeNotificationForSlack()
    {
    return config('slack.webhook_url');
    }
}