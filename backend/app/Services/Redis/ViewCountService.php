<?php

namespace App\Services\Redis;

use App\Models\Masters\Post;
use App\Models\Masters\PostView;
use Illuminate\Support\Facades\Redis;
use Carbon\Carbon;
use Illuminate\Support\Arr;

class ViewCountService
{
    const ipExpireSec = 300;

    const viewExpireSec = 3600*24;

    protected $postView;

    public function __construct( PostView $postView )
    {
        $this->postView =  $postView;
    }

    public function addCount( Post $post, $ip )
    {
        if( $this->ipViewLimit($post->id, $ip) ){
            $this->updateCacheViewCount($post->id);
        }
    }

    public function ipViewLimit($id, $ip)
    {
        //In redis, the key value segmentation is done with: which can be understood as the same as PHP's namespace
        $ipPostViewKey    = 'post:ip:limit:'.$id;
        //The redis command sismember checks whether the key exists in the set type, the time complexity of the instruction is O (1), and the value of the set type is unique
        $existsInRedisSet = Redis::command('SISMEMBER', [$ipPostViewKey, $ip]);
        if(!$existsInRedisSet){
            //Sadd, set type instruction, adds a value IP to the ippostviewkey
            Redis::command('SADD', [$ipPostViewKey, $ip]);
            //And set the lifetime of the key, here set 300 seconds, after 300 seconds, the same IP access will be regarded as a new view
            Redis::command('EXPIRE', [$ipPostViewKey, self::ipExpireSec]);
            return true;
        }

        return false;
    }

    public function updateCacheViewCount($id)
    {

        $date = Carbon::now()->format('Y-m-d');
        $key = "views-id:".$date."-".$id;

        if(!Redis::command('GET', [$key])){
            Redis::command('SET', [$key, '1']);
            Redis::command('EXPIRE', [$key, self::viewExpireSec]);
        } else {
            Redis::command('INCR', [$key]);
        }

    }

    public function saveCount()
    {
        $results = Redis::command('KEYS', ['views-id:*']);

        foreach($results as $result){

            $addData = [
                'date' => mb_substr($result, 9, 10),
                'post_id' => mb_substr($result, 20),
                'count' => Redis::command('GET', [$result]),
            ];

            $this->updateModelViewCount($addData);

            Redis::command('DEL', [$result]);
        }
    }

    public function updateModelViewCount($addData)
    {
        $postView = $this->postView::firstOrNew(Arr::except($addData, ['count']));

        $postView->view_count += $addData['count'];

        $postView->save();
    }
}