<?php

namespace App\Services\ViewCount;

use App\Models\Masters\Post;
use App\Models\Masters\PostView;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Cache;
use Carbon\Carbon;
use Illuminate\Support\Arr;

class ViewCountService
{
    const ipExpireSec = 300;

    const viewExpireSec = 3600*24;

    private $postView;

    private $dailyViewCount;

    public function __construct( PostView $postView )
    {
        $this->postView =  $postView;
        $this->dailyViewCount = collect();
    }

    public function addCount( Post $post, $ip )
    {
        if( $this->ipViewLimit($post->id, $ip) ){
            $this->saveCount($post->id);
        }
    }

    public function ipViewLimit($id, $ip)
    {
        $ipPostViewKey = $id.":".$ip;

        if( !Cache::has($ipPostViewKey) ){
            Cache::put($ipPostViewKey, true, self::ipExpireSec);
            return true;
        }
        return false;
    }

    public function saveCount($id)
    {
        $date = Carbon::now()->format('Y-m-d');

        $addData = [
            'date' => $date,
            'post_id' => $id,
        ];

        $this->updateModelViewCount($addData);
    }

    public function updateModelViewCount($addData)
    {
        $postView = $this->postView::firstOrNew($addData);

        $postView->view_count ++;

        $postView->save();
    }

}