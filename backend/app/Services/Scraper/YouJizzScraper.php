<?php

namespace App\Services\Scraper;

use App\Services\Scraper\Abstracts\VideoSiteScraper;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Output\ConsoleOutput;
use Illuminate\Support\Str;

class YouJizzScraper extends VideoSiteScraper
{
    protected $videoSiteId = 7;

    public function setTime($time){}

    public function getLastPage()
    {
        $lastPage;
        $crawler = $this->client->request('GET', 'https://www.youjizz.com/search/japan%20censored-20.html?');
        $crawler->filter('.pager-container li:nth-last-child(2)')->each(function ($node) use ( &$lastPage ) {
            $lastPage = (int) $node->text();
        });

        return $lastPage;
    }

    public function getPost()
    {
        $baseUrl = 'https://www.youjizz.com';
        $posts = [];
        $status = 200;
        $i = 1;

        $bar = new ProgressBar(new ConsoleOutput(), 0);

        while( true )
        {
            try {

                $crawler = $this->client->request('GET', 'https://www.youjizz.com/search/japan%20censored-'.$i.'.html?');
                $status = $this->client->getResponse()->getStatusCode();

                if( $status != 200 ) break;

                $result = $crawler->filter('.video-item')->each(function ($node) use (&$posts, &$baseUrl) {
                    $posts[] = [
                        'id' => (string) Str::uuid(),
                        'url' => $baseUrl.$node->filter('a')->attr('href'),
                        'thumbnail' => 'https:'.$node->filter('img')->attr('data-original'),
                        'time' => $node->filter('.video-content-wrapper .time')->text(),
                        'video_site_id' => $this->videoSite->id,
                        'created_at' => $this->createdTime,
                    ];
                });

                if( count($result) == 0 ) break;

                if( count($posts) >= 1000 ) {
                    $this->setPost($posts)->createPost()->deletePost();
                    $posts = [];
                }
                // var_dump($i.'/'.$this->lastPage);
                // var_dump("memory usage：". memory_get_usage() / (1024 * 1024) ."MB");
                // var_dump("memory peak usage：". memory_get_peak_usage() / (1024 * 1024) ."MB");

                if( $this->lastPage == $i ) break;
                $i++;

            } catch (\Exception $e) {
                report($e);
                $this->status = false;
                $this->failedCount++;

                if( $this->lastPage == $i ) break;
                $i++;
            }
            $bar->advance();
        }
        $this->setPost($posts)->createPost()->deletePost();

        $bar->finish();
        echo PHP_EOL;
    }
}