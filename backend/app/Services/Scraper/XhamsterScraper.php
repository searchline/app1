<?php

namespace App\Services\Scraper;

use App\Services\Scraper\Abstracts\VideoSiteScraper;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Output\ConsoleOutput;
use Illuminate\Support\Str;

class XhamsterScraper extends VideoSiteScraper
{
    protected $videoSiteId = 5;

    public function setTime($time){}

    public function getLastPage()
    {
        $lastPage;
        $crawler = $this->client->request('GET', 'https://jp.xhamster.com/categories/japanese/1');
        $crawler->filter('.pager-container li:nth-last-child(2)')->each(function ($node) use ( &$lastPage ) {
            $lastPage = $node->text();
            // var_dump($node->text());
            // var_dump($lastPage);
            $lastPage = str_replace(',', '',$lastPage);
            $lastPage = str_replace('.', '',$lastPage);
            $lastPage = (int) $lastPage;
        });
        return $lastPage;
    }

    public function getPost()
    {
        $baseUrl = '';
        $posts = [];

        $bar = new ProgressBar(new ConsoleOutput(), $this->lastPage);

        for( $i=1; $i <= $this->lastPage; $i++ )
        {
            try {

                $crawler = $this->client->request('GET', 'https://jp.xhamster.com/categories/japanese/'.$i);
                $crawler->filter('.thumb-list__item')->each(function ($node) use (&$posts, &$baseUrl) {

                    if( $node->filter('a')->count() && $node->filter('img')->count() && $node->filter('.thumb-image-container__duration span')->count() ){

                        $posts[] = [
                            'id' => (string) Str::uuid(),
                            'url' => $baseUrl.$node->filter('a')->attr('href'),
                            'thumbnail' => $node->filter('img')->attr('src'),
                            'time' => $node->filter('.thumb-image-container__duration span')->text(),
                            'video_site_id' => $this->videoSite->id,
                            'created_at' => $this->createdTime,
                        ];
                    }

                });

                if( count($posts) >= 1000 ) {
                    $this->setPost($posts)->createPost()->deletePost();
                    $posts = [];
                }

                if( $this->createCount >=5000 ) {
                    break;
                }
                // var_dump($i.'/'.$this->lastPage);
                // var_dump("memory usage：". memory_get_usage() / (1024 * 1024) ."MB");
                // var_dump("memory peak usage：". memory_get_peak_usage() / (1024 * 1024) ."MB");

            } catch (\Exception $e) {
                report($e);
                $this->status = false;
                $this->failedCount++;
            }
            $bar->advance();
        }
        $this->setPost($posts)->createPost()->deletePost();

        $bar->finish();
        echo PHP_EOL;
    }
}