<?php

namespace App\Services\Scraper;

use App\Services\Scraper\Abstracts\VideoSiteScraper;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Output\ConsoleOutput;
use Illuminate\Support\Str;

class RedTubeScraper extends VideoSiteScraper
{
    protected $videoSiteId = 6;

    public function setTime($time)
    {
        $time = str_replace(' ', '', $time);
        if(preg_match('/p/',$time)){
            $time = mb_strstr( $time, 'p', false);
            $time = str_replace('p', '', $time);
        }
        return $time;
    }

    public function getLastPage()
    {
        return 0;
    }

    public function getPost()
    {
        $baseUrl = 'https://jp.redtube.com';
        $posts = [];
        $status = 200;
        $i = 1;

        $bar = new ProgressBar(new ConsoleOutput(), 0);

        while( true )
        {
            try {

                $crawler = $this->client->request('GET', 'https://jp.redtube.com/?search=japanese+censored&page='.$i);
                $status = $this->client->getResponse()->getStatusCode();

                if( $status != 200 ) break;

                $crawler->filter('.videos_grid .video_block_wrapper:not(.js_is_premium_thumb)')->each(function ($node) use (&$posts, &$baseUrl) {
                    $posts[] = [
                        'id' => (string) Str::uuid(),
                        'url' => $baseUrl.$node->filter('a')->attr('href'),
                        'thumbnail' => $node->filter('img')->attr('data-src'),
                        'time' => $this->setTime($node->filter('a .duration:not(.video_quality)')->text()),
                        'video_site_id' => $this->videoSite->id,
                        'created_at' => $this->createdTime,
                    ];
                });

                if( count($posts) >= 1000 ) {
                    $this->setPost($posts)->createPost()->deletePost();
                    $posts = [];
                }
                // var_dump($i.'/'.$this->lastPage);
                // var_dump("memory usage：". memory_get_usage() / (1024 * 1024) ."MB");
                // var_dump("memory peak usage：". memory_get_peak_usage() / (1024 * 1024) ."MB");

                if( $this->lastPage == $i ) break;
                $i++;

            } catch (\Exception $e) {
                report($e);
                $this->status = false;
                $this->failedCount++;

                if( $this->lastPage == $i ) break;
                $i++;
            }
            $bar->advance();
        }
        $this->setPost($posts)->createPost()->deletePost();

        $bar->finish();
        echo PHP_EOL;
    }

}