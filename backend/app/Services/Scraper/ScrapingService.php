<?php

namespace App\Services\Scraper;

use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use Goutte\Client;
use Symfony\Component\HttpClient\HttpClient;

class ScrapingService
{
    private $createdTime;

    private $lastPage;

    public function __construct()
    {
        $this->createdTime = Carbon::now();

        if( app()->isProduction() ) { $this->lastPage = 0; } else { $this->lastPage = 1; }
    }

    public function runVideoSite()
    {
        // id:1
        app('xvideosScraper')->setLastPage($this->lastPage)->setCreatedTime($this->createdTime)->run();

        // id:2
        app('pornhubScraper')->setLastPage($this->lastPage)->setCreatedTime($this->createdTime)->run();

        // id:3
        app('javynowScraper')->setLastPage($this->lastPage)->setCreatedTime($this->createdTime)->run();

        // id:4
        app('sharevideosScraper')->setLastPage($this->lastPage)->setCreatedTime($this->createdTime)->run();

        // id:5
        app('xhamsterScraper')->setLastPage($this->lastPage)->setCreatedTime($this->createdTime)->run();

        // id:6
        app('redtubeScraper')->setLastPage($this->lastPage)->setCreatedTime($this->createdTime)->run();

        // id:7
        app('youjizzScraper')->setLastPage($this->lastPage)->setCreatedTime($this->createdTime)->run();

        // id:8
        app('spankingbangScraper')->setLastPage($this->lastPage)->setCreatedTime($this->createdTime)->run();

    }

    public function runXvideos()
    {
        // id:1
        app('xvideosScraper')->setLastPage($this->lastPage)->setCreatedTime($this->createdTime)->run();

    }

    public function runPornhub()
    {
        // id:2
        app('pornhubScraper')->setLastPage($this->lastPage)->setCreatedTime($this->createdTime)->run();
    }

    public function runJavynow()
    {
        // id:3
        app('javynowScraper')->setLastPage($this->lastPage)->setCreatedTime($this->createdTime)->run();
    }

    public function runSharevideos()
    {
        // id:4
        app('sharevideosScraper')->setLastPage($this->lastPage)->setCreatedTime($this->createdTime)->run();
    }

    public function runXhamster()
    {
        // id:5
        app('xhamsterScraper')->setLastPage($this->lastPage)->setCreatedTime($this->createdTime)->run();
    }

    public function runRedtube()
    {
        // id:6
        app('redtubeScraper')->setLastPage($this->lastPage)->setCreatedTime($this->createdTime)->run();
    }

    public function runYoujizz()
    {
        // id:7
        app('youjizzScraper')->setLastPage($this->lastPage)->setCreatedTime($this->createdTime)->run();
    }

    public function runSpankingbang()
    {
        // id:8
        app('spankingbangScraper')->setLastPage($this->lastPage)->setCreatedTime($this->createdTime)->run();
    }

    public function uploadImage( $post )
    {
        try {

            // $client = new Client(HttpClient::create(['verify_peer' => false, 'verify_host' => false]));
            // $crawler = $client->request('GET', $post->thumbnail);
            // $status  = $client->getResponse()->getStatusCode();

            // if( $status == 200 ) {
            // }

            // $fileName = uniqid("", true).'.jpg';

            // $options = stream_context_create(array('ssl' => array(
            //     'verify_peer'      => false,
            //     'verify_peer_name' => false
            // )));

            // Storage::disk('images')->put( $fileName, file_get_contents($post->thumbnail, false, $options) );


            $fileName = uniqid("", true).'.jpg';

            $option = [
                CURLOPT_URL => $post->thumbnail,
                CURLOPT_HEADER => false,
                CURLOPT_SSL_VERIFYPEER => false,
                CURLOPT_RETURNTRANSFER => true,
            ];

            $curl = curl_init();
            curl_setopt_array($curl, $option);

            $image   = curl_exec($curl);
            $info    = curl_getinfo($curl);
            $errorNo = curl_errno($curl);

            if ($errorNo == CURLE_OK) {
                if ($info['http_code'] == 200) {

                    Storage::disk('images')->put( $fileName, $image );

                    $post->filename = $fileName;

                    $post->save();
                }
            }
            curl_close($curl);

        } catch (\Exception $e) {
            report($e);
        }
    }

}