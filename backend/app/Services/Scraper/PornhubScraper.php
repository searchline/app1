<?php

namespace App\Services\Scraper;

use App\Services\Scraper\Abstracts\VideoSiteScraper;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Output\ConsoleOutput;
use Illuminate\Support\Str;

class PornhubScraper extends VideoSiteScraper
{
    protected $videoSiteId = 2;

    public function setTime($time){}

    public function getLastPage()
    {
        return 0;
    }

    public function getPost()
    {
        $baseUrl = 'https://jp.pornhub.com';
        $posts = [];

        $status = 200;
        $i = 1;

        $bar = new ProgressBar(new ConsoleOutput(), 0);

        while( true )
        {
            try {

                $crawler = $this->client->request('GET', 'https://jp.pornhub.com/video/search?search=censored&page='.$i);
                $status = $this->client->getResponse()->getStatusCode();

                if( $status != 200 ) break;

                $crawler->filter('#videoSearchResult .videoBox')->each(function ($node) use (&$posts, &$baseUrl) {
                    $posts[] = [
                        'id' => (string) Str::uuid(),
                        'url' => $baseUrl.$node->filter('.wrap a')->attr('href'),
                        'thumbnail' => $node->filter('.wrap img')->attr('data-src'),
                        'time' => $node->filter('.wrap .duration')->text(),
                        'video_site_id' => $this->videoSite->id,
                        'created_at' => $this->createdTime,
                    ];
                });

                if( count($posts) >= 1000 ) {
                    $this->setPost($posts)->createPost()->deletePost();
                    $posts = [];
                }

                // var_dump($i.'/'.$this->lastPage);
                // var_dump("memory usage：". memory_get_usage() / (1024 * 1024) ."MB");
                // var_dump("memory peak usage：". memory_get_peak_usage() / (1024 * 1024) ."MB");

                if( $this->lastPage == $i ) break;
                $i++;

            } catch (\Exception $e) {
                report($e);
                $this->status = false;
                $this->failedCount++;

                if( $this->lastPage == $i ) break;
                $i++;
            }
            $bar->advance();
        }
        $this->setPost($posts)->createPost()->deletePost();

        $bar->finish();
        echo PHP_EOL;
    }
}