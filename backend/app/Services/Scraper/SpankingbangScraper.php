<?php

namespace App\Services\Scraper;

use App\Services\Scraper\Abstracts\VideoSiteScraper;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Output\ConsoleOutput;
use Illuminate\Support\Str;

class SpankingbangScraper extends VideoSiteScraper
{
    protected $videoSiteId = 8;

    public function setTime($time){}

    public function getLastPage()
    {
        $lastPage = 0;
        $crawler = $this->client->request('GET', 'https://jp.spankbang.com/s/japanese+censored/1/');
        $crawler->filter('.pagination ul li:nth-of-type(6)')->each(function ($node) use ( &$lastPage ) {
            $lastPage = (int) $node->text();
        });

        return $lastPage;
    }

    public function getPost()
    {
        $baseUrl = 'https://jp.spankbang.com';
        $posts = [];

        $bar = new ProgressBar(new ConsoleOutput(), $this->lastPage);

        for( $i=1; $i <= $this->lastPage; $i++ )
        {
            try {

                $crawler = $this->client->request('GET', 'https://jp.spankbang.com/s/japanese+censored/'.$i);
                $result = $crawler->filter('.video-list-with-ads .video-item')->each(function ($node) use (&$posts, &$baseUrl, &$count) {

                    if( $node->filter('a')->eq(0)->count() )
                    {
                        // if(mb_strlen($baseUrl.$node->filter('a')->eq(0)->attr('href')) > 255) {
                        //     echo mb_strlen($baseUrl.$node->filter('a')->eq(0)->attr('href')).PHP_EOL;
                        // }
                        $time = $node->filter('.thumb .t .l')->text();
                        $time = str_replace( 'm', '', $time).':00';
                        $posts[] = [
                            'id' => (string) Str::uuid(),
                            'url' => $baseUrl.$node->filter('a')->eq(0)->attr('href'),
                            'thumbnail' => $node->filter('img')->attr('data-src'),
                            'time' => $time,
                            'video_site_id' => $this->videoSite->id,
                            'created_at' => $this->createdTime,
                        ];
                    }
                });

                if( count($posts) >= 1000 ) {
                    $this->setPost($posts)->createPost()->deletePost();
                    $posts = [];
                }
                // var_dump($i.'/'.$this->lastPage);
                // var_dump( count($result) );
                // if( count($result) == 0 ) break;
                // var_dump("memory usage：". memory_get_usage() / (1024 * 1024) ."MB");
                // var_dump("memory peak usage：". memory_get_peak_usage() / (1024 * 1024) ."MB");

            } catch (\Exception $e) {
                report($e);
                $this->status = false;
                $this->failedCount++;
            }
            $bar->advance();
            sleep(5);
        }
        $this->setPost($posts)->createPost()->deletePost();

        $bar->finish();
        echo PHP_EOL;
    }
}