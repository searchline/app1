<?php

namespace App\Services\Scraper;

use App\Models\Masters\Post;
use Illuminate\Support\Facades\Storage;
use App\Services\Scraper\Abstracts\VideoSiteScraper;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Output\ConsoleOutput;
use Carbon\Carbon;
use Goutte\Client;
use Symfony\Component\HttpClient\HttpClient;
use Illuminate\Support\Str;

class ShareVideosScraper extends VideoSiteScraper
{
    protected $videoSiteId = 4;

    public function run()
    {
        if(is_null($this->videoSite) || !$this->videoSite->scrape ) {
            return ;
        }

        $this->setClient();

        if(empty($this->createdTime)) {
            $this->createdTime = Carbon::now();
        }

        if(empty($this->lastPage)) {
            $this->setLastPage($this->getLastPage());
        }

        $this->getPost();

        $this->createGetPostHistory();
    }

    public function setClient()
    {
        $this->client = new Client(HttpClient::create(['verify_peer' => false, 'verify_host' => false]));
    }


    public function setTime($time)
    {
        $minutes = mb_strstr($time, '分', true);

        if(strlen($minutes) > 1){
            $minutes = str_pad($minutes, strlen($minutes), 0, STR_PAD_LEFT);
        } else {
            $minutes = str_pad($minutes, 2, 0, STR_PAD_LEFT);
        }

        return $minutes.':00';
    }

    public function getLastPage()
    {
        $lastPage = 0;
        try {
            $crawler = $this->client->request('GET', 'https://share-videos.se/view/new?uid=13&page=1');
            $status = $this->client->getResponse()->getStatusCode();

            $crawler->filter('#pager #pager_last a')->each(function ($node) use ( &$lastPage ) {
                $lastPage = (int) $node->text();
            });
        } catch (\Exception $e) {
            report($e);
        }

        return $lastPage;
    }

    public function getPost()
    {
        $baseUrl = 'https://share-videos.se';
        $posts = [];

        $bar = new ProgressBar(new ConsoleOutput(), $this->lastPage);

        for( $i=1; $i <= $this->lastPage; $i++ )
        {
            try {
                $crawler = $this->client->request('GET', 'https://share-videos.se/view/new?uid=13&page='.$i);
                $crawler->filter('.video_post')->each(function ($node) use (&$posts, &$baseUrl) {
                    $posts[] = [
                        'id' => (string) Str::uuid(),
                        'url' => $baseUrl.$node->filter('.video-item_con a')->attr('href'),
                        'thumbnail' => $node->filter('.video-item_con img')->attr('src'),
                        'time' => $this->setTime(str_replace(' ', '', $node->filter('.media-length')->text())),
                        'video_site_id' => $this->videoSite->id,
                        'created_at' => $this->createdTime,
                    ];
                });

                if( count($posts) >= 1000 ) {
                    $this->setPost($posts)->createPost()->deletePost();
                    $posts = [];
                }
                // var_dump($i.'/'.$this->lastPage);
                // var_dump("memory usage：". memory_get_usage() / (1024 * 1024) ."MB");
                // var_dump("memory peak usage：". memory_get_peak_usage() / (1024 * 1024) ."MB");

            } catch (\Exception $e) {
                report($e);
                $this->status = false;
                $this->failedCount++;
            }
            $bar->advance();
        }
        $this->setPost($posts)->createPost()->deletePost();

        $bar->finish();
        echo PHP_EOL;
    }

    // public function createPost()
    // {
    //     $this->posts->each(function ($item, $key) {
    //         try {
    //             $post = Post::firstOrNew([
    //                 'url' => $item['url'],
    //                 'time' => $item['time'],
    //                 'video_site_id' => $this->videoSite->id,
    //             ]);

    //             if (!$post->exists) {

    //                 $crawler = $this->client->request('GET', $item['url']);
    //                 $urlStatus = $this->client->getResponse()->getStatusCode();

    //                 $crawler = $this->client->request('GET', $item['thumbnail']);
    //                 $thumbnailStatus = $this->client->getResponse()->getStatusCode();

    //                 if( $urlStatus == 200 && $thumbnailStatus == 200 ) {

    //                     $fileName = uniqid("", true).'.jpg';

    //                     $options = stream_context_create(array('ssl' => array(
    //                         'verify_peer'      => false,
    //                         'verify_peer_name' => false
    //                     )));

    //                     Storage::disk('images')->put( $fileName, file_get_contents($item['thumbnail'], false, $options));

    //                     $post->filename = $fileName;
    //                     $post->created_at = $this->createdTime;
    //                     $post->save();
    //                     $this->createCount++;
    //                 }
    //             }

    //         } catch (\Exception $e) {
    //             report($e);
    //             $this->status = false;
    //             $this->failedCount++;
    //         }
    //     });

    //     return $this;
    // }
}