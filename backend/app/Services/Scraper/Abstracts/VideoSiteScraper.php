<?php

namespace App\Services\Scraper\Abstracts;

use App\Models\Masters\Post;
use App\Models\Masters\VideoSite;
use App\Models\Histories\GetPostHistory;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Collection;
use Goutte\Client;

abstract class VideoSiteScraper
{
    protected $client;

    protected $videoSite;

    protected $lastPage = 0;

    protected $createdTime = null;

    protected $posts;

    protected $createCount = 0;

    protected $failedCount = 0;

    protected $status = true;

    public function __construct( VideoSite $videoSite )
    {
        $this->videoSite = $videoSite::find($this->videoSiteId);
        $this->client = new Client();
    }

    abstract public function setTime($time);
    abstract public function getPost();
    abstract public function getLastPage();

    public function run()
    {
        if(is_null($this->videoSite) || !$this->videoSite->scrape ) {
            return ;
        }

        if(empty($this->createdTime)) {
            $this->createdTime = Carbon::now();
        }

        if(empty($this->lastPage)) {
            $this->setLastPage($this->getLastPage());
        }

        $this->getPost();

        $this->createGetPostHistory();

        // $this->sendSlack();
    }


    public function setPost( $posts )
    {
        $this->posts = $posts;

        return $this;
    }

    public function deletePost()
    {
        $this->posts = [];

        return $this;
    }

    public function setLastPage( $lastPage )
    {
        $this->lastPage = $lastPage;

        return $this;
    }

    public function setCreatedTime( $createdTime )
    {
        $this->createdTime = $createdTime;

        return $this;
    }

    public function setCreateCount()
    {
        $posts = Post::where('created_at', $this->createdTime)
                    ->where('video_site_id', $this->videoSite->id)
                    ->get();

        $this->createCount = count($posts);

        return $this;
    }

    public function createPost()
    {
        Post::upsert($this->posts, ['url'], []);

        $this->setCreateCount();

        return $this;
    }

    // public function createPost()
    // {
    //     // $this->posts->each(function ($item, $key) {
    //     // });
    //     $i = 0;
    //     foreach( $this->posts->chunk(500) as $items ) {

    //         foreach( $items as $item ) {

    //             try {
    //                 $post = Post::firstOrNew([
    //                     'url' => $item['url'],
    //                     'time' => $item['time'],
    //                     'video_site_id' => $this->videoSite->id,
    //                 ]);

    //                 if (!$post->exists) {

    //                     $crawler = $this->client->request('GET', $item['url']);
    //                     $urlStatus = $this->client->getResponse()->getStatusCode();

    //                     $crawler = $this->client->request('GET', $item['thumbnail']);
    //                     $thumbnailStatus = $this->client->getResponse()->getStatusCode();

    //                     if( $urlStatus == 200 && $thumbnailStatus == 200 ) {

    //                         $fileName = uniqid("", true).'.jpg';

    //                         // Storage::disk('images')->put( $fileName, file_get_contents($item['thumbnail']));

    //                         $post->filename = $fileName;
    //                         $post->created_at = $this->createdTime;
    //                         $post->save();
    //                         $this->createCount++;
    //                     } else {
    //                         var_dump($urlStatus);
    //                         var_dump($thumbnailStatus);
    //                         var_dump($item['url']);
    //                         var_dump($item['thumbnail']);
    //                         $i++;
    //                         trim(fgets(STDIN));
    //                     }
    //                 }

    //             } catch (\Exception $e) {
    //                 report($e);
    //                 $this->status = false;
    //                 $this->failedCount++;
    //             }

    //             var_dump($item);
    //             // var_dump("memory usage：". memory_get_usage() / (1024 * 1024) ."MB");
    //             // var_dump("memory peak usage：". memory_get_peak_usage() / (1024 * 1024) ."MB");
    //         }
    //     }
    //     var_dump('status error'.$i);
    //     return $this;
    // }

    public function createGetPostHistory()
    {
        if(!empty($this->createCount)){
            $getPostHistory = GetPostHistory::create([
                'date' => $this->createdTime,
                'count' => $this->createCount,
                'failed_count' => $this->failedCount,
                'status' => $this->status,
                'video_site_id' => $this->videoSite->id,
            ]);

            Post::where([
                ['created_at', '=', $this->createdTime],
                ['video_site_id', '=', $this->videoSite->id],
                ])
                ->update([ 'get_post_history_id' => $getPostHistory->id ]);
        }
    }

    public function sendSlack()
    {
        if(!empty($this->createCount)){
            \Slack::send(
                "==============================="."\n".
                "記事取得通知"."\n".
                "==============================="."\n".
                "\n".
                "サービス名：".$this->videoSite->name."\n".
                "取得件数：".$this->createCount."\n".
                "時間：".$this->createdTime."\n".
                "ステータス：".$this->status."\n".
                "\n"
            );
        }
    }
}