<?php

namespace App\Services\Scraper;

use App\Services\Scraper\Abstracts\VideoSiteScraper;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Output\ConsoleOutput;
use Illuminate\Support\Str;

class XvideosScraper extends VideoSiteScraper
{
    protected $videoSiteId = 1;

    public function setTime($time)
    {
        if(preg_match('/h/',$time)){
            $hour = mb_strstr($time, 'h', true);
            $minutes = mb_strstr($time, 'h', false);
            $minutes = mb_strstr(str_replace('h', '', $minutes), 'min', true);

            $minutes = $hour*60+$minutes;
            $minutes = str_pad($minutes, strlen($minutes), 0, STR_PAD_LEFT);
            $adjTime = $minutes.':00';

        } elseif(preg_match('/sec/',$time)) {
            $second = mb_strstr($time, 'sec', true);
            $second = str_pad($second, 2, 0, STR_PAD_LEFT);
            $adjTime = '00:'.$second;

        } else {
            $minutes = mb_strstr($time, 'min', true);
            $minutes = str_pad($minutes, 2, 0, STR_PAD_LEFT);
            $adjTime = $minutes.':00';

        }

        return $adjTime;
    }

    public function getLastPage()
    {
        $lastPage;
        $crawler = $this->client->request('GET', 'https://www.xvideos.com/?k=censored&p=0');
        $crawler->filter('.pagination ul li:nth-last-child(2)')->each(function ($node) use ( &$lastPage ) {
            $lastPage = str_replace(',','',$node->text());
        });

        return $lastPage;
    }

    public function getPost()
    {
        $baseUrl = 'https://www.xvideos.com';
        $posts = [];

        $bar = new ProgressBar(new ConsoleOutput(), $this->lastPage);

        for( $i=0; $i < $this->lastPage; $i++ )
        {
            try {

                $crawler =$this->client->request('GET', 'https://www.xvideos.com/?k=censored&p='.$i);
                $crawler->filter('.thumb-block')->each(function ($node) use (&$posts, &$baseUrl) {
                    $posts[] = [
                        'id' => (string) Str::uuid(),
                        'url' => $baseUrl.$node->filter('.thumb a')->attr('href'),
                        'thumbnail' => $node->filter('.thumb img')->attr('data-src'),
                        'time' => $this->setTime(str_replace(' ', '', $node->filter('.metadata .duration')->text())),
                        'video_site_id' => $this->videoSite->id,
                        'created_at' => $this->createdTime,
                    ];
                });

                if( count($posts) >= 1000 ) {
                    $this->setPost($posts)->createPost()->deletePost();
                    $posts = [];
                }
                // var_dump($i.'/'.$this->lastPage);
                // var_dump("memory usage：". memory_get_usage() / (1024 * 1024) ."MB");
                // var_dump("memory peak usage：". memory_get_peak_usage() / (1024 * 1024) ."MB");

            } catch (\Exception $e) {
                report($e);
                $this->status = false;
                $this->failedCount++;
            }
            $bar->advance();
        }
        $this->setPost($posts)->createPost()->deletePost();

        $bar->finish();
        echo PHP_EOL;
    }
}