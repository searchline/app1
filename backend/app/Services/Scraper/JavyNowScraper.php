<?php

namespace App\Services\Scraper;

use App\Services\Scraper\Abstracts\VideoSiteScraper;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Output\ConsoleOutput;
use Illuminate\Support\Str;

class JavyNowScraper extends VideoSiteScraper
{
    protected $videoSiteId = 3;

    public function setTime($time){}

    public function getLastPage()
    {
        $lastPage;
        $crawler = $this->client->request('GET', 'https://javynow.com/?p=1');
        $crawler->filter('.pager__item:nth-of-type(3)')->each(function ($node) use ( &$lastPage ) {
            $lastPage = (int) str_replace(',','',$node->text());
        });
        return $lastPage;
    }

    public function getPost()
    {
        $baseUrl = 'https://javynow.com';
        $posts = [];

        $bar = new ProgressBar(new ConsoleOutput(), $this->lastPage);

        for( $i=1; $i <= $this->lastPage; $i++ ) {

            try {

                $crawler = $this->client->request('GET', 'https://javynow.com/?p='.$i);
                $crawler->filter('.video__thumb')->each(function ($node) use (&$posts, &$baseUrl) {
                    $posts[] = [
                        'id' => (string) Str::uuid(),
                        'url' => $baseUrl.$node->filter('a')->attr('href'),
                        'thumbnail' => 'https:'.$node->filter('img')->attr('data-src'),
                        'time' => $node->filter('.video__duration__value')->text(),
                        'video_site_id' => $this->videoSite->id,
                        'created_at' => $this->createdTime,
                    ];
                });

                if( count($posts) >= 1000 ) {
                    $this->setPost($posts)->createPost()->deletePost();
                    $posts = [];
                }

                if( $this->createCount >=5000 ) {
                    break;
                }
                // var_dump($i.'/'.$this->lastPage);
                // var_dump("memory usage：". memory_get_usage() / (1024 * 1024) ."MB");
                // var_dump("memory peak usage：". memory_get_peak_usage() / (1024 * 1024) ."MB");

            } catch (\Exception $e) {
                report($e);
                $this->status = false;
                $this->failedCount++;
            }
            $bar->advance();
        }
        $this->setPost($posts)->createPost()->deletePost();

        $bar->finish();
        echo PHP_EOL;
    }
}