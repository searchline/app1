<?php

namespace App\Services\Scraper;

use Illuminate\Support\Facades\Facade;

class Scraper extends Facade
{
  protected static function getFacadeAccessor()
  {
    return 'scrapingService';
  }
}
