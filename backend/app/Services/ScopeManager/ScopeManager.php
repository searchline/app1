<?php

namespace App\Services\ScopeManager;

class ScopeManager
{

    protected $isActive;

    public function setActive(bool $bool)
    {
        $this->isActive = $bool;
    }

    public function getActive()
    {
        return $this->isActive;
    }
}